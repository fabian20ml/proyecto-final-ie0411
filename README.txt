**************************PROYECTO FINAL MICROELECTRONICA IE-0411**************************
-------------------------------------------------------------------------------------------------------
JOSE FABIAN MARCENARO LARIOS
C: B64069
-------------------------------------------------------------------------------------------------------
DESCRIPCION

Se realizo la descripción y verificación conductual y estructural de un contador de 4 bits con los 
con 5 modos de operacion, posteriormente se repitio el procedimiento con un contador de 32 bit, con la
especificacion de que no se podian implementar bloques always@(), solo asignaciones e instanciasd de 
los contadores de 4 bits. Por ultimo se hizo un analisis de timing y de sintesis fisica de ambos modulos
para tecnologias osu035 y osu050.
-------------------------------------------------------------------------------------------------------
DISENO Y VERIFICACION

INSTRUCCIONES DE USO

Existen 2 directorios, uno contiene toda la estructura de descripcion y verificacion de los modulos
contadores de 4 y 32 bits, cada uno con su makefile, pero la estructura es la misma.

Los siguientes comandos deberan ser ejecutados en la terminal de linux para compilar y correr el programa
exitosamente (uso del MAKEFILE).

make: compila el archivo tb_top.v que contiene todos los modulos necesarios para la verificacion,
     ejecuta el binario y abre gtkwave.

make compile: compila el archivo tb_top.v que contiene todos los modulos.

make run:   ejecuta el binario, crea el archivo .vcd necesario para abrir la simulacion en gtkwave
            y crea el archivo .log que es el resultado de la verificacion que hace el checker al 
            comparar las salidas del scoreboard contra las del modulo en cuestion.

make gtk: abre la simulacion en gtkwave

make clean: borra archivos binarios y logs.

make yosysScript: ejecuta el script de yosys que sintetiza los modulos contador.v

make sedCommand: se ejecuta despues de sintetizar los modulos contador, para cambiar los nombres de
                los modulos, y no halla confusiones en el testbench.
-------------------------------------------------------------------------------------------------------
PRUEBAS REALIZADAS

Las pruebas creadas se encuentran en el archivo driver.v como tasks. La prueba default es la prueba 1,
si se quisiera cambiar de prueba, se modifica la seccion fork del archivo contador_tb.v que contiene el
llamado al task y se cambia por el nombre del task correspondiente. Las pruebas se explican brevemente 
a continuacion:

+ Prueba0:  se habilita el modulo (enable = 1) durante la simulacion, mientras que la entrada "modo" 
            se asigna en 0 en todo el transcurso de la simulacion para que el contador cuente de 
            manera ascendente de 1 en 1.

+ Prueba1:  se habilita el modulo (enable = 1) durante la simulacion, mientras que la entrada "modo" 
            se asigna en 1 en todo el transcurso de la simulacion para que el contador cuente de 
            manera descendente de 1 en 1.

+ Prueba2:  se habilita el modulo (enable = 1) durante la simulacion, mientras que la entrada "modo" 
            se asigna en 2 en todo el transcurso de la simulacion para que el contador cuente de 
            manera descendente de 3 en 3.

+ Prueba3:  se habilita el modulo (enable = 1) durante la simulacion, mientras que la entrada "modo" 
            se asigna en 3 en todo el transcurso de la simulacion para que el contador pase a la
            salida "Q" lo que sea que tenga en la entrada "D".

+ Prueba4: se habilita el modulo (enable = 1) durante la simulacion, pero la entrada modo cambia de 
           manera aleatoria.

+ Prueba5:  todas las entradas cambian aleatoriamente.

-------------------------------------------------------------------------------------------------------
OTRAS CONSIDERACIONES

En el tb_top.v se encuentran instanciados los contadores conductual y sintetizado, pues las simulaciones fueron analizadas
en gtkwave de modo que fueran las salidas de todos los contadores contra las del scoreboard. El codigo 
actualmente tiene comentados la instancia del modulo conductual, para que el checker y el archivo .log
compare el scoreboard vs. el contador sintetizado en la Prueba2. Si se quiere comprobar como compararia el checker
otro contador, basta con comentar la instancia del contador sintetizado y descomentar  el otro contador.

Si se quiere cambiar de prueba, se debe ir a la linea 32 o 33 de los archivos contador_tb.v dependiendo
de si se trata de contador de 4 o 32 bits.

Para mas detalles, revisar el informe adjunto con esta entrega.

-------------------------------------------------------------------------------------------------------
QFLOW

################################################################################

Se sintetizaron 2 modulos: contador_cond.v y contador32_cond.v y se utilizaron 
las tecnologias de qflow osu035 y osu050 para comparar resultados en los disenos.

Hay 2 directorios, uno para el contador de 4 bits, y otro para el de 32 bits, cada
uno con dos subdirectorios osu035 y osu050.

En el directorio osu035 se encuentran dos subdirectorios llamados arbiter y uart, 
donde se encuentran todos los recursos necesarios para sintetizar y ver parametros
de estos modulos utilizando tech osu035.

En el directorio osu050 se encuentran dos subdirectorios llamados arbiter y uart, 
donde se encuentran todos los recursos necesarios para sintetizar y ver parametros
de estos modulos utilizando tech osu050.

################################################################################

NOTA: todos los archivos ya estan compilados y sintetizados, es decir no se 
necesita hacer uso del makefile, sin embargo, si se deseara probarlo, se tiene 
que leer sobre las excepciones del makefile, que se describen mas abajo en este
README.

################################################################################

En cada directorio de arbiter y uart dentro de ParteA y ParteB, se encuentra un 
makefile con el siguiente uso:

+ make: intentara realizar todo el proceso de sintetizacion y desplegar estadisticas
        del diseno.
    
+ make synth: sintetizara el diseno, pero este comando por lo general debe utilizarse
              varias veces, hasta que el proceso concluya exitosamente.

+ make chageTech: en el caso de que se necesite usar osu050, se necesita cambiar ese
                  parametro en el archivo qflow_vars.sh de cada directorio correspondiente
                  este comando lo hace.

+ make qflowExec: este comando sirve para ejecutar "paso a paso" los comandos del archivo
                  qflow_exec.sh, sin embargo no servira, pues la ruta absoluta de los 
                  archivos sera diferente dependiendo de el equipo donde se quiera correr
                  el makefile, por lo que los makefiles que contengan este comando tampoco
                  funcionaran al correr solo "make" o  bien "make qflowExec".

+ make stadistics: funciona para desplegar las estadisticas del diseno, una vez que todo 
                    este sintetizado, listo y sin errores.

################################################################################