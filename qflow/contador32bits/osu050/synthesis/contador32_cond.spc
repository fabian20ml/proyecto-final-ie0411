*SPICE netlist created from BLIF module contador32_cond by blif2BSpice
.include /usr/share/qflow/tech/osu050/osu050_stdcells.sp
.subckt contador32_cond vdd gnd clk enable D<0> D<1> D<2> D<3> D<4> D<5> D<6> D<7> D<8> D<9> D<10> D<11> D<12> D<13> D<14> D<15> D<16> D<17> D<18> D<19> D<20> D<21> D<22> D<23> D<24> D<25> D<26> D<27> D<28> D<29> D<30> D<31> modo<0> modo<1> modo<2> reset Q<0> Q<1> Q<2> Q<3> Q<4> Q<5> Q<6> Q<7> Q<8> Q<9> Q<10> Q<11> Q<12> Q<13> Q<14> Q<15> Q<16> Q<17> Q<18> Q<19> Q<20> Q<21> Q<22> Q<23> Q<24> Q<25> Q<26> Q<27> Q<28> Q<29> Q<30> Q<31> rco 
XBUFX4_1 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf6 BUFX4
XBUFX4_2 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf5 BUFX4
XBUFX2_1 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf4 BUFX2
XBUFX4_3 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf3 BUFX4
XBUFX4_4 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf2 BUFX4
XBUFX4_5 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf1 BUFX4
XBUFX4_6 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf0 BUFX4
XBUFX4_7 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf5 BUFX4
XBUFX2_2 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf4 BUFX2
XBUFX2_3 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf3 BUFX2
XBUFX2_4 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf2 BUFX2
XBUFX4_8 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf1 BUFX4
XBUFX4_9 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf0 BUFX4
XBUFX4_10 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf5 BUFX4
XBUFX4_11 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf4 BUFX4
XBUFX4_12 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf3 BUFX4
XBUFX2_5 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf2 BUFX2
XBUFX2_6 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf1 BUFX2
XBUFX4_13 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf0 BUFX4
XBUFX2_7 vdd gnd enable enable_bF$buf4 BUFX2
XBUFX2_8 vdd gnd enable enable_bF$buf3 BUFX2
XBUFX2_9 vdd gnd enable enable_bF$buf2 BUFX2
XBUFX2_10 vdd gnd enable enable_bF$buf1 BUFX2
XBUFX2_11 vdd gnd enable enable_bF$buf0 BUFX2
XBUFX4_14 vdd gnd reset reset_bF$buf6 BUFX4
XBUFX4_15 vdd gnd reset reset_bF$buf5 BUFX4
XBUFX4_16 vdd gnd reset reset_bF$buf4 BUFX4
XBUFX4_17 vdd gnd reset reset_bF$buf3 BUFX4
XBUFX4_18 vdd gnd reset reset_bF$buf2 BUFX4
XBUFX4_19 vdd gnd reset reset_bF$buf1 BUFX4
XBUFX4_20 vdd gnd reset reset_bF$buf0 BUFX4
XNAND3X1_1 _343_ vdd gnd _342_ _347_ _348_ NAND3X1
XOAI21X1_1 gnd vdd _292_ _335_ _349_ _307_ OAI21X1
XAND2X2_1 vdd gnd _349_ _333_ _350_ AND2X2
XOAI21X1_2 gnd vdd _350_ _348_ _351_ _320_ OAI21X1
XNAND2X1_1 vdd _352_ gnd _314_ _21_<19> NAND2X1
XNAND2X1_2 vdd _287_<3> gnd _352_ _351_ NAND2X1
XDFFPOSX1_1 vdd _287_<0> gnd counter5.Q_clk<0> clk5 DFFPOSX1
XDFFPOSX1_2 vdd _287_<1> gnd counter5.Q_clk<1> clk5 DFFPOSX1
XDFFPOSX1_3 vdd _287_<2> gnd counter5.Q_clk<2> clk5 DFFPOSX1
XDFFPOSX1_4 vdd _287_<3> gnd counter5.Q_clk<3> clk5 DFFPOSX1
XDFFPOSX1_5 vdd _288_ gnd counter5.rco_clk clk5 DFFPOSX1
XINVX1_1 counter6.Q_clk<0> _355_ vdd gnd INVX1
XNOR2X1_1 vdd _355_ gnd _21_<20> reset_bF$buf6 NOR2X1
XINVX1_2 counter6.Q_clk<1> _356_ vdd gnd INVX1
XNOR2X1_2 vdd _356_ gnd _21_<21> reset_bF$buf5 NOR2X1
XINVX1_3 counter6.Q_clk<2> _357_ vdd gnd INVX1
XNOR2X1_3 vdd _357_ gnd _21_<22> reset_bF$buf4 NOR2X1
XINVX1_4 counter6.Q_clk<3> _358_ vdd gnd INVX1
XNOR2X1_4 vdd _358_ gnd _21_<23> reset_bF$buf3 NOR2X1
XINVX1_5 counter6.rco_clk _359_ vdd gnd INVX1
XNOR2X1_5 vdd _359_ gnd counter6.rco reset_bF$buf2 NOR2X1
XAND2X2_2 vdd gnd counter6.Q_clk<0> counter6.Q_clk<1> _360_ AND2X2
XNAND3X1_2 counter6.Q_clk<3> vdd gnd counter6.Q_clk<2> _360_ _361_ NAND3X1
XNOR3X1_1 vdd gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf5 counter2.modo_2_bF$buf6 _362_ NOR3X1
XINVX1_6 _362_ _363_ vdd gnd INVX1
XNOR2X1_6 vdd _363_ gnd _364_ _361_ NOR2X1
XNOR2X1_7 vdd counter2.modo_0_bF$buf4 gnd _365_ counter2.modo_2_bF$buf5 NOR2X1
XNAND2X1_3 vdd _366_ gnd counter2.modo_1_bF$buf4 _365_ NAND2X1
XNOR2X1_8 vdd counter6.Q_clk<3> gnd _367_ counter6.Q_clk<2> NOR2X1
XOAI21X1_3 gnd vdd _355_ _356_ _368_ _367_ OAI21X1
XINVX1_7 counter2.modo_2_bF$buf4 _369_ vdd gnd INVX1
XINVX1_8 counter2.modo_1_bF$buf3 _370_ vdd gnd INVX1
XNAND3X1_3 _369_ vdd gnd counter2.modo_0_bF$buf3 _370_ _371_ NAND3X1
XNOR2X1_9 vdd counter6.Q_clk<1> gnd _372_ counter6.Q_clk<0> NOR2X1
XNAND2X1_4 vdd _373_ gnd _367_ _372_ NAND2X1
XOAI22X1_1 gnd vdd _368_ _366_ _371_ _373_ _374_ OAI22X1
XOAI21X1_4 gnd vdd _374_ _364_ _375_ enable_bF$buf4 OAI21X1
XINVX1_9 counter2.modo_0_bF$buf2 _376_ vdd gnd INVX1
XNAND3X1_4 _376_ vdd gnd enable_bF$buf3 _370_ _377_ NAND3X1
XNAND2X1_5 vdd _378_ gnd enable_bF$buf2 _369_ NAND2X1
XNAND3X1_5 _378_ vdd gnd counter6.rco_clk _377_ _379_ NAND3X1
XAOI21X1_1 gnd vdd _375_ _379_ _354_ reset_bF$buf1 AOI21X1
XINVX1_10 enable_bF$buf1 _380_ vdd gnd INVX1
XNAND2X1_6 vdd _381_ gnd _380_ _21_<20> NAND2X1
XNAND2X1_7 vdd _382_ gnd counter2.modo_0_bF$buf1 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_10 vdd _382_ gnd _383_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_11 vdd counter2.modo_2_bF$buf2 gnd _384_ counter6.Q_clk<0> NOR2X1
XAOI22X1_1 gnd vdd _383_ D<20> _385_ _382_ _384_ AOI22X1
XNOR2X1_12 vdd _378_ gnd _386_ reset_bF$buf0 NOR2X1
XINVX1_11 _386_ _387_ vdd gnd INVX1
XOAI21X1_5 gnd vdd _385_ _387_ _353_<0> _381_ OAI21X1
XNOR2X1_13 vdd _360_ gnd _388_ _372_ NOR2X1
XNAND2X1_8 vdd _389_ gnd D<21> _383_ NAND2X1
XOAI21X1_6 gnd vdd _371_ _388_ _390_ _389_ OAI21X1
XAOI21X1_2 gnd vdd _365_ _388_ _391_ _390_ AOI21X1
XNAND2X1_9 vdd _392_ gnd _380_ _21_<21> NAND2X1
XOAI21X1_7 gnd vdd _387_ _391_ _353_<1> _392_ OAI21X1
XNAND2X1_10 vdd _393_ gnd counter6.Q_clk<0> counter6.Q_clk<1> NAND2X1
XXNOR2X1_1 _393_ counter6.Q_clk<2> gnd vdd _394_ XNOR2X1
XNAND3X1_6 counter6.Q_clk<1> vdd gnd counter6.Q_clk<0> counter6.Q_clk<2> _395_ NAND3X1
XOAI21X1_8 gnd vdd _355_ _356_ _396_ _357_ OAI21X1
XAOI22X1_2 gnd vdd _396_ _395_ _397_ counter2.modo_1_bF$buf1 _365_ AOI22X1
XAOI21X1_3 gnd vdd _363_ _394_ _398_ _397_ AOI21X1
XNOR3X1_2 vdd gnd counter2.modo_1_bF$buf0 _376_ counter2.modo_2_bF$buf1 _399_ NOR3X1
XAOI21X1_4 gnd vdd _355_ _356_ _400_ _357_ AOI21X1
XNOR3X1_3 vdd gnd counter6.Q_clk<1> counter6.Q_clk<2> counter6.Q_clk<0> _401_ NOR3X1
XOAI21X1_9 gnd vdd _400_ _401_ _402_ _399_ OAI21X1
XNAND2X1_11 vdd _403_ gnd D<22> _383_ NAND2X1
XNAND2X1_12 vdd _404_ gnd _403_ _402_ NAND2X1
XOAI21X1_10 gnd vdd _404_ _398_ _405_ _386_ OAI21X1
XNAND2X1_13 vdd _406_ gnd _380_ _21_<22> NAND2X1
XNAND2X1_14 vdd _353_<2> gnd _406_ _405_ NAND2X1
XOAI21X1_11 gnd vdd _357_ _393_ _407_ _358_ OAI21X1
XNAND3X1_7 _407_ vdd gnd _362_ _361_ _408_ NAND3X1
XNAND2X1_15 vdd _409_ gnd D<23> _383_ NAND2X1
XNOR3X1_4 vdd gnd counter2.modo_0_bF$buf0 _370_ counter2.modo_2_bF$buf0 _410_ NOR3X1
XOAI21X1_12 gnd vdd counter6.Q_clk<2> _360_ _411_ _358_ OAI21X1
XNAND3X1_8 _357_ vdd gnd counter6.Q_clk<3> _393_ _412_ NAND3X1
XNAND3X1_9 _410_ vdd gnd _412_ _411_ _413_ NAND3X1
XNAND3X1_10 _409_ vdd gnd _408_ _413_ _414_ NAND3X1
XOAI21X1_13 gnd vdd _358_ _401_ _415_ _373_ OAI21X1
XAND2X2_3 vdd gnd _415_ _399_ _416_ AND2X2
XOAI21X1_14 gnd vdd _416_ _414_ _417_ _386_ OAI21X1
XNAND2X1_16 vdd _418_ gnd _380_ _21_<23> NAND2X1
XNAND2X1_17 vdd _353_<3> gnd _418_ _417_ NAND2X1
XDFFPOSX1_6 vdd _353_<0> gnd counter6.Q_clk<0> clk6 DFFPOSX1
XDFFPOSX1_7 vdd _353_<1> gnd counter6.Q_clk<1> clk6 DFFPOSX1
XDFFPOSX1_8 vdd _353_<2> gnd counter6.Q_clk<2> clk6 DFFPOSX1
XDFFPOSX1_9 vdd _353_<3> gnd counter6.Q_clk<3> clk6 DFFPOSX1
XDFFPOSX1_10 vdd _354_ gnd counter6.rco_clk clk6 DFFPOSX1
XINVX1_12 counter7.Q_clk<0> _421_ vdd gnd INVX1
XNOR2X1_14 vdd _421_ gnd _21_<24> reset_bF$buf6 NOR2X1
XINVX1_13 counter7.Q_clk<1> _422_ vdd gnd INVX1
XNOR2X1_15 vdd _422_ gnd _21_<25> reset_bF$buf5 NOR2X1
XINVX1_14 counter7.Q_clk<2> _423_ vdd gnd INVX1
XNOR2X1_16 vdd _423_ gnd _21_<26> reset_bF$buf4 NOR2X1
XINVX1_15 counter7.Q_clk<3> _424_ vdd gnd INVX1
XNOR2X1_17 vdd _424_ gnd _21_<27> reset_bF$buf3 NOR2X1
XINVX1_16 counter7.rco_clk _425_ vdd gnd INVX1
XNOR2X1_18 vdd _425_ gnd counter7.rco reset_bF$buf2 NOR2X1
XAND2X2_4 vdd gnd counter7.Q_clk<0> counter7.Q_clk<1> _426_ AND2X2
XNAND3X1_11 counter7.Q_clk<3> vdd gnd counter7.Q_clk<2> _426_ _427_ NAND3X1
XNOR3X1_5 vdd gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf5 counter2.modo_2_bF$buf6 _428_ NOR3X1
XINVX1_17 _428_ _429_ vdd gnd INVX1
XNOR2X1_19 vdd _429_ gnd _430_ _427_ NOR2X1
XNOR2X1_20 vdd counter2.modo_0_bF$buf4 gnd _431_ counter2.modo_2_bF$buf5 NOR2X1
XNAND2X1_18 vdd _432_ gnd counter2.modo_1_bF$buf4 _431_ NAND2X1
XNOR2X1_21 vdd counter7.Q_clk<3> gnd _433_ counter7.Q_clk<2> NOR2X1
XOAI21X1_15 gnd vdd _421_ _422_ _434_ _433_ OAI21X1
XINVX1_18 counter2.modo_2_bF$buf4 _435_ vdd gnd INVX1
XINVX1_19 counter2.modo_1_bF$buf3 _436_ vdd gnd INVX1
XNAND3X1_12 _435_ vdd gnd counter2.modo_0_bF$buf3 _436_ _437_ NAND3X1
XNOR2X1_22 vdd counter7.Q_clk<1> gnd _438_ counter7.Q_clk<0> NOR2X1
XNAND2X1_19 vdd _439_ gnd _433_ _438_ NAND2X1
XOAI22X1_2 gnd vdd _434_ _432_ _437_ _439_ _440_ OAI22X1
XOAI21X1_16 gnd vdd _440_ _430_ _441_ enable_bF$buf0 OAI21X1
XINVX1_20 counter2.modo_0_bF$buf2 _442_ vdd gnd INVX1
XNAND3X1_13 _442_ vdd gnd enable_bF$buf4 _436_ _443_ NAND3X1
XNAND2X1_20 vdd _444_ gnd enable_bF$buf3 _435_ NAND2X1
XNAND3X1_14 _444_ vdd gnd counter7.rco_clk _443_ _445_ NAND3X1
XAOI21X1_5 gnd vdd _441_ _445_ _420_ reset_bF$buf1 AOI21X1
XINVX1_21 enable_bF$buf2 _446_ vdd gnd INVX1
XNAND2X1_21 vdd _447_ gnd _446_ _21_<24> NAND2X1
XNAND2X1_22 vdd _448_ gnd counter2.modo_0_bF$buf1 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_23 vdd _448_ gnd _449_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_24 vdd counter2.modo_2_bF$buf2 gnd _450_ counter7.Q_clk<0> NOR2X1
XAOI22X1_3 gnd vdd _449_ D<24> _451_ _448_ _450_ AOI22X1
XNOR2X1_25 vdd _444_ gnd _452_ reset_bF$buf0 NOR2X1
XINVX1_22 _452_ _453_ vdd gnd INVX1
XOAI21X1_17 gnd vdd _451_ _453_ _419_<0> _447_ OAI21X1
XNOR2X1_26 vdd _426_ gnd _454_ _438_ NOR2X1
XNAND2X1_23 vdd _455_ gnd D<25> _449_ NAND2X1
XOAI21X1_18 gnd vdd _437_ _454_ _456_ _455_ OAI21X1
XAOI21X1_6 gnd vdd _431_ _454_ _457_ _456_ AOI21X1
XNAND2X1_24 vdd _458_ gnd _446_ _21_<25> NAND2X1
XOAI21X1_19 gnd vdd _453_ _457_ _419_<1> _458_ OAI21X1
XNAND2X1_25 vdd _459_ gnd counter7.Q_clk<0> counter7.Q_clk<1> NAND2X1
XXNOR2X1_2 _459_ counter7.Q_clk<2> gnd vdd _460_ XNOR2X1
XNAND3X1_15 counter7.Q_clk<1> vdd gnd counter7.Q_clk<0> counter7.Q_clk<2> _461_ NAND3X1
XOAI21X1_20 gnd vdd _421_ _422_ _462_ _423_ OAI21X1
XAOI22X1_4 gnd vdd _462_ _461_ _463_ counter2.modo_1_bF$buf1 _431_ AOI22X1
XAOI21X1_7 gnd vdd _429_ _460_ _464_ _463_ AOI21X1
XNOR3X1_6 vdd gnd counter2.modo_1_bF$buf0 _442_ counter2.modo_2_bF$buf1 _465_ NOR3X1
XAOI21X1_8 gnd vdd _421_ _422_ _466_ _423_ AOI21X1
XNOR3X1_7 vdd gnd counter7.Q_clk<1> counter7.Q_clk<2> counter7.Q_clk<0> _467_ NOR3X1
XOAI21X1_21 gnd vdd _466_ _467_ _468_ _465_ OAI21X1
XNAND2X1_26 vdd _469_ gnd D<26> _449_ NAND2X1
XNAND2X1_27 vdd _470_ gnd _469_ _468_ NAND2X1
XOAI21X1_22 gnd vdd _470_ _464_ _471_ _452_ OAI21X1
XNAND2X1_28 vdd _472_ gnd _446_ _21_<26> NAND2X1
XNAND2X1_29 vdd _419_<2> gnd _472_ _471_ NAND2X1
XOAI21X1_23 gnd vdd _423_ _459_ _473_ _424_ OAI21X1
XNAND3X1_16 _473_ vdd gnd _428_ _427_ _474_ NAND3X1
XNAND2X1_30 vdd _475_ gnd D<27> _449_ NAND2X1
XNOR3X1_8 vdd gnd counter2.modo_0_bF$buf0 _436_ counter2.modo_2_bF$buf0 _476_ NOR3X1
XOAI21X1_24 gnd vdd counter7.Q_clk<2> _426_ _477_ _424_ OAI21X1
XNAND3X1_17 _423_ vdd gnd counter7.Q_clk<3> _459_ _478_ NAND3X1
XNAND3X1_18 _476_ vdd gnd _478_ _477_ _479_ NAND3X1
XNAND3X1_19 _475_ vdd gnd _474_ _479_ _480_ NAND3X1
XOAI21X1_25 gnd vdd _424_ _467_ _481_ _439_ OAI21X1
XAND2X2_5 vdd gnd _481_ _465_ _482_ AND2X2
XOAI21X1_26 gnd vdd _482_ _480_ _483_ _452_ OAI21X1
XNAND2X1_31 vdd _484_ gnd _446_ _21_<27> NAND2X1
XNAND2X1_32 vdd _419_<3> gnd _484_ _483_ NAND2X1
XDFFPOSX1_11 vdd _419_<0> gnd counter7.Q_clk<0> clk7 DFFPOSX1
XDFFPOSX1_12 vdd _419_<1> gnd counter7.Q_clk<1> clk7 DFFPOSX1
XDFFPOSX1_13 vdd _419_<2> gnd counter7.Q_clk<2> clk7 DFFPOSX1
XDFFPOSX1_14 vdd _419_<3> gnd counter7.Q_clk<3> clk7 DFFPOSX1
XDFFPOSX1_15 vdd _420_ gnd counter7.rco_clk clk7 DFFPOSX1
XINVX1_23 counter8.Q_clk<0> _487_ vdd gnd INVX1
XNOR2X1_27 vdd _487_ gnd _21_<28> reset_bF$buf6 NOR2X1
XINVX1_24 counter8.Q_clk<1> _488_ vdd gnd INVX1
XNOR2X1_28 vdd _488_ gnd _21_<29> reset_bF$buf5 NOR2X1
XINVX1_25 counter8.Q_clk<2> _489_ vdd gnd INVX1
XNOR2X1_29 vdd _489_ gnd _21_<30> reset_bF$buf4 NOR2X1
XINVX1_26 counter8.Q_clk<3> _490_ vdd gnd INVX1
XNOR2X1_30 vdd _490_ gnd _21_<31> reset_bF$buf3 NOR2X1
XINVX1_27 counter8.rco_clk _491_ vdd gnd INVX1
XNOR2X1_31 vdd _491_ gnd counter8.rco reset_bF$buf2 NOR2X1
XAND2X2_6 vdd gnd counter8.Q_clk<0> counter8.Q_clk<1> _492_ AND2X2
XNAND3X1_20 counter8.Q_clk<3> vdd gnd counter8.Q_clk<2> _492_ _493_ NAND3X1
XNOR3X1_9 vdd gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf5 counter2.modo_2_bF$buf6 _494_ NOR3X1
XINVX1_28 _494_ _495_ vdd gnd INVX1
XNOR2X1_32 vdd _495_ gnd _496_ _493_ NOR2X1
XNOR2X1_33 vdd counter2.modo_0_bF$buf4 gnd _497_ counter2.modo_2_bF$buf5 NOR2X1
XNAND2X1_33 vdd _498_ gnd counter2.modo_1_bF$buf4 _497_ NAND2X1
XNOR2X1_34 vdd counter8.Q_clk<3> gnd _499_ counter8.Q_clk<2> NOR2X1
XOAI21X1_27 gnd vdd _487_ _488_ _500_ _499_ OAI21X1
XINVX1_29 counter2.modo_2_bF$buf4 _501_ vdd gnd INVX1
XINVX1_30 counter2.modo_1_bF$buf3 _502_ vdd gnd INVX1
XNAND3X1_21 _501_ vdd gnd counter2.modo_0_bF$buf3 _502_ _503_ NAND3X1
XNOR2X1_35 vdd counter8.Q_clk<1> gnd _504_ counter8.Q_clk<0> NOR2X1
XNAND2X1_34 vdd _505_ gnd _499_ _504_ NAND2X1
XOAI22X1_3 gnd vdd _500_ _498_ _503_ _505_ _506_ OAI22X1
XOAI21X1_28 gnd vdd _506_ _496_ _507_ enable_bF$buf1 OAI21X1
XINVX1_31 counter2.modo_0_bF$buf2 _508_ vdd gnd INVX1
XNAND3X1_22 _508_ vdd gnd enable_bF$buf0 _502_ _509_ NAND3X1
XNAND2X1_35 vdd _510_ gnd enable_bF$buf4 _501_ NAND2X1
XNAND3X1_23 _510_ vdd gnd counter8.rco_clk _509_ _511_ NAND3X1
XAOI21X1_9 gnd vdd _507_ _511_ _486_ reset_bF$buf1 AOI21X1
XINVX1_32 enable_bF$buf3 _512_ vdd gnd INVX1
XNAND2X1_36 vdd _513_ gnd _512_ _21_<28> NAND2X1
XNAND2X1_37 vdd _514_ gnd counter2.modo_0_bF$buf1 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_36 vdd _514_ gnd _515_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_37 vdd counter2.modo_2_bF$buf2 gnd _516_ counter8.Q_clk<0> NOR2X1
XAOI22X1_5 gnd vdd _515_ D<28> _517_ _514_ _516_ AOI22X1
XNOR2X1_38 vdd _510_ gnd _518_ reset_bF$buf0 NOR2X1
XINVX1_33 _518_ _519_ vdd gnd INVX1
XOAI21X1_29 gnd vdd _517_ _519_ _485_<0> _513_ OAI21X1
XNOR2X1_39 vdd _492_ gnd _520_ _504_ NOR2X1
XNAND2X1_38 vdd _521_ gnd D<29> _515_ NAND2X1
XOAI21X1_30 gnd vdd _503_ _520_ _522_ _521_ OAI21X1
XAOI21X1_10 gnd vdd _497_ _520_ _523_ _522_ AOI21X1
XNAND2X1_39 vdd _524_ gnd _512_ _21_<29> NAND2X1
XOAI21X1_31 gnd vdd _519_ _523_ _485_<1> _524_ OAI21X1
XNAND2X1_40 vdd _525_ gnd counter8.Q_clk<0> counter8.Q_clk<1> NAND2X1
XXNOR2X1_3 _525_ counter8.Q_clk<2> gnd vdd _526_ XNOR2X1
XNAND3X1_24 counter8.Q_clk<1> vdd gnd counter8.Q_clk<0> counter8.Q_clk<2> _527_ NAND3X1
XOAI21X1_32 gnd vdd _487_ _488_ _528_ _489_ OAI21X1
XAOI22X1_6 gnd vdd _528_ _527_ _529_ counter2.modo_1_bF$buf1 _497_ AOI22X1
XAOI21X1_11 gnd vdd _495_ _526_ _530_ _529_ AOI21X1
XNOR3X1_10 vdd gnd counter2.modo_1_bF$buf0 _508_ counter2.modo_2_bF$buf1 _531_ NOR3X1
XAOI21X1_12 gnd vdd _487_ _488_ _532_ _489_ AOI21X1
XNOR3X1_11 vdd gnd counter8.Q_clk<1> counter8.Q_clk<2> counter8.Q_clk<0> _533_ NOR3X1
XOAI21X1_33 gnd vdd _532_ _533_ _534_ _531_ OAI21X1
XNAND2X1_41 vdd _535_ gnd D<30> _515_ NAND2X1
XNAND2X1_42 vdd _536_ gnd _535_ _534_ NAND2X1
XOAI21X1_34 gnd vdd _536_ _530_ _537_ _518_ OAI21X1
XNAND2X1_43 vdd _538_ gnd _512_ _21_<30> NAND2X1
XNAND2X1_44 vdd _485_<2> gnd _538_ _537_ NAND2X1
XOAI21X1_35 gnd vdd _489_ _525_ _539_ _490_ OAI21X1
XNAND3X1_25 _539_ vdd gnd _494_ _493_ _540_ NAND3X1
XNAND2X1_45 vdd _541_ gnd D<31> _515_ NAND2X1
XNOR3X1_12 vdd gnd counter2.modo_0_bF$buf0 _502_ counter2.modo_2_bF$buf0 _542_ NOR3X1
XOAI21X1_36 gnd vdd counter8.Q_clk<2> _492_ _543_ _490_ OAI21X1
XNAND3X1_26 _489_ vdd gnd counter8.Q_clk<3> _525_ _544_ NAND3X1
XNAND3X1_27 _542_ vdd gnd _544_ _543_ _545_ NAND3X1
XNAND3X1_28 _541_ vdd gnd _540_ _545_ _546_ NAND3X1
XOAI21X1_37 gnd vdd _490_ _533_ _547_ _505_ OAI21X1
XAND2X2_7 vdd gnd _547_ _531_ _548_ AND2X2
XOAI21X1_38 gnd vdd _548_ _546_ _549_ _518_ OAI21X1
XNAND2X1_46 vdd _550_ gnd _512_ _21_<31> NAND2X1
XNAND2X1_47 vdd _485_<3> gnd _550_ _549_ NAND2X1
XDFFPOSX1_16 vdd _485_<0> gnd counter8.Q_clk<0> clk8 DFFPOSX1
XDFFPOSX1_17 vdd _485_<1> gnd counter8.Q_clk<1> clk8 DFFPOSX1
XDFFPOSX1_18 vdd _485_<2> gnd counter8.Q_clk<2> clk8 DFFPOSX1
XDFFPOSX1_19 vdd _485_<3> gnd counter8.Q_clk<3> clk8 DFFPOSX1
XDFFPOSX1_20 vdd _486_ gnd counter8.rco_clk clk8 DFFPOSX1
XINVX1_34 modo<1> _0_ vdd gnd INVX1
XINVX1_35 modo<0> _1_ vdd gnd INVX1
XNAND2X1_48 vdd counter2.modo<0> gnd _0_ _1_ NAND2X1
XNAND2X1_49 vdd _2_ gnd modo<1> modo<0> NAND2X1
XINVX8_1 vdd gnd _2_ counter2.modo<1> INVX8
XINVX1_36 modo<2> _3_ vdd gnd INVX1
XAOI21X1_13 gnd vdd _1_ modo<1> counter2.modo<2> _3_ AOI21X1
XNOR2X1_40 vdd reset_bF$buf6 gnd _4_ modo<2> NOR2X1
XNAND3X1_29 _2_ vdd gnd counter1.rco _4_ _5_ NAND3X1
XINVX1_37 reset_bF$buf5 _6_ vdd gnd INVX1
XNAND3X1_30 _6_ vdd gnd _3_ _2_ _7_ NAND3X1
XNAND2X1_50 vdd _8_ gnd clk _7_ NAND2X1
XNAND2X1_51 vdd clk2 gnd _5_ _8_ NAND2X1
XNAND3X1_31 _2_ vdd gnd counter2.rco _4_ _9_ NAND3X1
XNAND2X1_52 vdd clk3 gnd _9_ _8_ NAND2X1
XNAND3X1_32 _2_ vdd gnd counter3.rco _4_ _10_ NAND3X1
XNAND2X1_53 vdd clk4 gnd _10_ _8_ NAND2X1
XNAND3X1_33 _2_ vdd gnd counter4.rco _4_ _11_ NAND3X1
XNAND2X1_54 vdd clk5 gnd _11_ _8_ NAND2X1
XNAND3X1_34 _2_ vdd gnd counter5.rco _4_ _12_ NAND3X1
XNAND2X1_55 vdd clk6 gnd _12_ _8_ NAND2X1
XNAND3X1_35 _2_ vdd gnd counter6.rco _4_ _13_ NAND3X1
XNAND2X1_56 vdd clk7 gnd _13_ _8_ NAND2X1
XNAND3X1_36 _2_ vdd gnd counter7.rco _4_ _14_ NAND3X1
XNAND2X1_57 vdd clk8 gnd _14_ _8_ NAND2X1
XNAND2X1_58 vdd _15_ gnd counter5.rco counter6.rco NAND2X1
XNAND2X1_59 vdd _16_ gnd counter7.rco counter8.rco NAND2X1
XNOR2X1_41 vdd _16_ gnd _17_ _15_ NOR2X1
XNAND2X1_60 vdd _18_ gnd counter1.rco counter2.rco NAND2X1
XNAND2X1_61 vdd _19_ gnd counter3.rco counter4.rco NAND2X1
XNOR2X1_42 vdd _19_ gnd _20_ _18_ NOR2X1
XAND2X2_8 vdd gnd _17_ _20_ _22_ AND2X2
XBUFX2_12 vdd gnd _21_<0> Q<0> BUFX2
XBUFX2_13 vdd gnd _21_<1> Q<1> BUFX2
XBUFX2_14 vdd gnd _21_<2> Q<2> BUFX2
XBUFX2_15 vdd gnd _21_<3> Q<3> BUFX2
XBUFX2_16 vdd gnd _21_<4> Q<4> BUFX2
XBUFX2_17 vdd gnd _21_<5> Q<5> BUFX2
XBUFX2_18 vdd gnd _21_<6> Q<6> BUFX2
XBUFX2_19 vdd gnd _21_<7> Q<7> BUFX2
XBUFX2_20 vdd gnd _21_<8> Q<8> BUFX2
XBUFX2_21 vdd gnd _21_<9> Q<9> BUFX2
XBUFX2_22 vdd gnd _21_<10> Q<10> BUFX2
XBUFX2_23 vdd gnd _21_<11> Q<11> BUFX2
XBUFX2_24 vdd gnd _21_<12> Q<12> BUFX2
XBUFX2_25 vdd gnd _21_<13> Q<13> BUFX2
XBUFX2_26 vdd gnd _21_<14> Q<14> BUFX2
XBUFX2_27 vdd gnd _21_<15> Q<15> BUFX2
XBUFX2_28 vdd gnd _21_<16> Q<16> BUFX2
XBUFX2_29 vdd gnd _21_<17> Q<17> BUFX2
XBUFX2_30 vdd gnd _21_<18> Q<18> BUFX2
XBUFX2_31 vdd gnd _21_<19> Q<19> BUFX2
XBUFX2_32 vdd gnd _21_<20> Q<20> BUFX2
XBUFX2_33 vdd gnd _21_<21> Q<21> BUFX2
XBUFX2_34 vdd gnd _21_<22> Q<22> BUFX2
XBUFX2_35 vdd gnd _21_<23> Q<23> BUFX2
XBUFX2_36 vdd gnd _21_<24> Q<24> BUFX2
XBUFX2_37 vdd gnd _21_<25> Q<25> BUFX2
XBUFX2_38 vdd gnd _21_<26> Q<26> BUFX2
XBUFX2_39 vdd gnd _21_<27> Q<27> BUFX2
XBUFX2_40 vdd gnd _21_<28> Q<28> BUFX2
XBUFX2_41 vdd gnd _21_<29> Q<29> BUFX2
XBUFX2_42 vdd gnd _21_<30> Q<30> BUFX2
XBUFX2_43 vdd gnd _21_<31> Q<31> BUFX2
XBUFX2_44 vdd gnd _22_ rco BUFX2
XINVX1_38 counter1.Q_clk<0> _25_ vdd gnd INVX1
XNOR2X1_43 vdd _25_ gnd _21_<0> reset_bF$buf4 NOR2X1
XINVX1_39 counter1.Q_clk<1> _26_ vdd gnd INVX1
XNOR2X1_44 vdd _26_ gnd _21_<1> reset_bF$buf3 NOR2X1
XINVX1_40 counter1.Q_clk<2> _27_ vdd gnd INVX1
XNOR2X1_45 vdd _27_ gnd _21_<2> reset_bF$buf2 NOR2X1
XINVX1_41 counter1.Q_clk<3> _28_ vdd gnd INVX1
XNOR2X1_46 vdd _28_ gnd _21_<3> reset_bF$buf1 NOR2X1
XINVX1_42 counter1.rco_clk _29_ vdd gnd INVX1
XNOR2X1_47 vdd _29_ gnd counter1.rco reset_bF$buf0 NOR2X1
XAND2X2_9 vdd gnd counter1.Q_clk<0> counter1.Q_clk<1> _30_ AND2X2
XNAND3X1_37 counter1.Q_clk<3> vdd gnd counter1.Q_clk<2> _30_ _31_ NAND3X1
XNOR3X1_13 vdd gnd modo<0> modo<1> modo<2> _32_ NOR3X1
XINVX1_43 _32_ _33_ vdd gnd INVX1
XNOR2X1_48 vdd _33_ gnd _34_ _31_ NOR2X1
XNOR2X1_49 vdd modo<0> gnd _35_ modo<2> NOR2X1
XNAND2X1_62 vdd _36_ gnd modo<1> _35_ NAND2X1
XNOR2X1_50 vdd counter1.Q_clk<3> gnd _37_ counter1.Q_clk<2> NOR2X1
XOAI21X1_39 gnd vdd _25_ _26_ _38_ _37_ OAI21X1
XINVX1_44 modo<2> _39_ vdd gnd INVX1
XINVX1_45 modo<1> _40_ vdd gnd INVX1
XNAND3X1_38 _39_ vdd gnd modo<0> _40_ _41_ NAND3X1
XNOR2X1_51 vdd counter1.Q_clk<1> gnd _42_ counter1.Q_clk<0> NOR2X1
XNAND2X1_63 vdd _43_ gnd _37_ _42_ NAND2X1
XOAI22X1_4 gnd vdd _38_ _36_ _41_ _43_ _44_ OAI22X1
XOAI21X1_40 gnd vdd _44_ _34_ _45_ enable_bF$buf2 OAI21X1
XINVX1_46 modo<0> _46_ vdd gnd INVX1
XNAND3X1_39 _46_ vdd gnd enable_bF$buf1 _40_ _47_ NAND3X1
XNAND2X1_64 vdd _48_ gnd enable_bF$buf0 _39_ NAND2X1
XNAND3X1_40 _48_ vdd gnd counter1.rco_clk _47_ _49_ NAND3X1
XAOI21X1_14 gnd vdd _45_ _49_ _24_ reset_bF$buf6 AOI21X1
XINVX1_47 enable_bF$buf4 _50_ vdd gnd INVX1
XNAND2X1_65 vdd _51_ gnd _50_ _21_<0> NAND2X1
XNAND2X1_66 vdd _52_ gnd modo<0> modo<1> NAND2X1
XNOR2X1_52 vdd _52_ gnd _53_ modo<2> NOR2X1
XNOR2X1_53 vdd modo<2> gnd _54_ counter1.Q_clk<0> NOR2X1
XAOI22X1_7 gnd vdd _53_ D<0> _55_ _52_ _54_ AOI22X1
XNOR2X1_54 vdd _48_ gnd _56_ reset_bF$buf5 NOR2X1
XINVX1_48 _56_ _57_ vdd gnd INVX1
XOAI21X1_41 gnd vdd _55_ _57_ _23_<0> _51_ OAI21X1
XNOR2X1_55 vdd _30_ gnd _58_ _42_ NOR2X1
XNAND2X1_67 vdd _59_ gnd D<1> _53_ NAND2X1
XOAI21X1_42 gnd vdd _41_ _58_ _60_ _59_ OAI21X1
XAOI21X1_15 gnd vdd _35_ _58_ _61_ _60_ AOI21X1
XNAND2X1_68 vdd _62_ gnd _50_ _21_<1> NAND2X1
XOAI21X1_43 gnd vdd _57_ _61_ _23_<1> _62_ OAI21X1
XNAND2X1_69 vdd _63_ gnd counter1.Q_clk<0> counter1.Q_clk<1> NAND2X1
XXNOR2X1_4 _63_ counter1.Q_clk<2> gnd vdd _64_ XNOR2X1
XNAND3X1_41 counter1.Q_clk<1> vdd gnd counter1.Q_clk<0> counter1.Q_clk<2> _65_ NAND3X1
XOAI21X1_44 gnd vdd _25_ _26_ _66_ _27_ OAI21X1
XAOI22X1_8 gnd vdd _66_ _65_ _67_ modo<1> _35_ AOI22X1
XAOI21X1_16 gnd vdd _33_ _64_ _68_ _67_ AOI21X1
XNOR3X1_14 vdd gnd modo<1> _46_ modo<2> _69_ NOR3X1
XAOI21X1_17 gnd vdd _25_ _26_ _70_ _27_ AOI21X1
XNOR3X1_15 vdd gnd counter1.Q_clk<1> counter1.Q_clk<2> counter1.Q_clk<0> _71_ NOR3X1
XOAI21X1_45 gnd vdd _70_ _71_ _72_ _69_ OAI21X1
XNAND2X1_70 vdd _73_ gnd D<2> _53_ NAND2X1
XNAND2X1_71 vdd _74_ gnd _73_ _72_ NAND2X1
XOAI21X1_46 gnd vdd _74_ _68_ _75_ _56_ OAI21X1
XNAND2X1_72 vdd _76_ gnd _50_ _21_<2> NAND2X1
XNAND2X1_73 vdd _23_<2> gnd _76_ _75_ NAND2X1
XOAI21X1_47 gnd vdd _27_ _63_ _77_ _28_ OAI21X1
XNAND3X1_42 _77_ vdd gnd _32_ _31_ _78_ NAND3X1
XNAND2X1_74 vdd _79_ gnd D<3> _53_ NAND2X1
XNOR3X1_16 vdd gnd modo<0> _40_ modo<2> _80_ NOR3X1
XOAI21X1_48 gnd vdd counter1.Q_clk<2> _30_ _81_ _28_ OAI21X1
XNAND3X1_43 _27_ vdd gnd counter1.Q_clk<3> _63_ _82_ NAND3X1
XNAND3X1_44 _80_ vdd gnd _82_ _81_ _83_ NAND3X1
XNAND3X1_45 _79_ vdd gnd _78_ _83_ _84_ NAND3X1
XOAI21X1_49 gnd vdd _28_ _71_ _85_ _43_ OAI21X1
XAND2X2_10 vdd gnd _85_ _69_ _86_ AND2X2
XOAI21X1_50 gnd vdd _86_ _84_ _87_ _56_ OAI21X1
XNAND2X1_75 vdd _88_ gnd _50_ _21_<3> NAND2X1
XNAND2X1_76 vdd _23_<3> gnd _88_ _87_ NAND2X1
XDFFPOSX1_21 vdd _23_<0> gnd counter1.Q_clk<0> clk DFFPOSX1
XDFFPOSX1_22 vdd _23_<1> gnd counter1.Q_clk<1> clk DFFPOSX1
XDFFPOSX1_23 vdd _23_<2> gnd counter1.Q_clk<2> clk DFFPOSX1
XDFFPOSX1_24 vdd _23_<3> gnd counter1.Q_clk<3> clk DFFPOSX1
XDFFPOSX1_25 vdd _24_ gnd counter1.rco_clk clk DFFPOSX1
XINVX1_49 counter2.Q_clk<0> _91_ vdd gnd INVX1
XNOR2X1_56 vdd _91_ gnd _21_<4> reset_bF$buf4 NOR2X1
XINVX1_50 counter2.Q_clk<1> _92_ vdd gnd INVX1
XNOR2X1_57 vdd _92_ gnd _21_<5> reset_bF$buf3 NOR2X1
XINVX1_51 counter2.Q_clk<2> _93_ vdd gnd INVX1
XNOR2X1_58 vdd _93_ gnd _21_<6> reset_bF$buf2 NOR2X1
XINVX1_52 counter2.Q_clk<3> _94_ vdd gnd INVX1
XNOR2X1_59 vdd _94_ gnd _21_<7> reset_bF$buf1 NOR2X1
XINVX1_53 counter2.rco_clk _95_ vdd gnd INVX1
XNOR2X1_60 vdd _95_ gnd counter2.rco reset_bF$buf0 NOR2X1
XAND2X2_11 vdd gnd counter2.Q_clk<0> counter2.Q_clk<1> _96_ AND2X2
XNAND3X1_46 counter2.Q_clk<3> vdd gnd counter2.Q_clk<2> _96_ _97_ NAND3X1
XNOR3X1_17 vdd gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf5 counter2.modo_2_bF$buf6 _98_ NOR3X1
XINVX1_54 _98_ _99_ vdd gnd INVX1
XNOR2X1_61 vdd _99_ gnd _100_ _97_ NOR2X1
XNOR2X1_62 vdd counter2.modo_0_bF$buf4 gnd _101_ counter2.modo_2_bF$buf5 NOR2X1
XNAND2X1_77 vdd _102_ gnd counter2.modo_1_bF$buf4 _101_ NAND2X1
XNOR2X1_63 vdd counter2.Q_clk<3> gnd _103_ counter2.Q_clk<2> NOR2X1
XOAI21X1_51 gnd vdd _91_ _92_ _104_ _103_ OAI21X1
XINVX1_55 counter2.modo_2_bF$buf4 _105_ vdd gnd INVX1
XINVX1_56 counter2.modo_1_bF$buf3 _106_ vdd gnd INVX1
XNAND3X1_47 _105_ vdd gnd counter2.modo_0_bF$buf3 _106_ _107_ NAND3X1
XNOR2X1_64 vdd counter2.Q_clk<1> gnd _108_ counter2.Q_clk<0> NOR2X1
XNAND2X1_78 vdd _109_ gnd _103_ _108_ NAND2X1
XOAI22X1_5 gnd vdd _104_ _102_ _107_ _109_ _110_ OAI22X1
XOAI21X1_52 gnd vdd _110_ _100_ _111_ enable_bF$buf3 OAI21X1
XINVX1_57 counter2.modo_0_bF$buf2 _112_ vdd gnd INVX1
XNAND3X1_48 _112_ vdd gnd enable_bF$buf2 _106_ _113_ NAND3X1
XNAND2X1_79 vdd _114_ gnd enable_bF$buf1 _105_ NAND2X1
XNAND3X1_49 _114_ vdd gnd counter2.rco_clk _113_ _115_ NAND3X1
XAOI21X1_18 gnd vdd _111_ _115_ _90_ reset_bF$buf6 AOI21X1
XINVX1_58 enable_bF$buf0 _116_ vdd gnd INVX1
XNAND2X1_80 vdd _117_ gnd _116_ _21_<4> NAND2X1
XNAND2X1_81 vdd _118_ gnd counter2.modo_0_bF$buf1 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_65 vdd _118_ gnd _119_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_66 vdd counter2.modo_2_bF$buf2 gnd _120_ counter2.Q_clk<0> NOR2X1
XAOI22X1_9 gnd vdd _119_ D<4> _121_ _118_ _120_ AOI22X1
XNOR2X1_67 vdd _114_ gnd _122_ reset_bF$buf5 NOR2X1
XINVX1_59 _122_ _123_ vdd gnd INVX1
XOAI21X1_53 gnd vdd _121_ _123_ _89_<0> _117_ OAI21X1
XNOR2X1_68 vdd _96_ gnd _124_ _108_ NOR2X1
XNAND2X1_82 vdd _125_ gnd D<5> _119_ NAND2X1
XOAI21X1_54 gnd vdd _107_ _124_ _126_ _125_ OAI21X1
XAOI21X1_19 gnd vdd _101_ _124_ _127_ _126_ AOI21X1
XNAND2X1_83 vdd _128_ gnd _116_ _21_<5> NAND2X1
XOAI21X1_55 gnd vdd _123_ _127_ _89_<1> _128_ OAI21X1
XNAND2X1_84 vdd _129_ gnd counter2.Q_clk<0> counter2.Q_clk<1> NAND2X1
XXNOR2X1_5 _129_ counter2.Q_clk<2> gnd vdd _130_ XNOR2X1
XNAND3X1_50 counter2.Q_clk<1> vdd gnd counter2.Q_clk<0> counter2.Q_clk<2> _131_ NAND3X1
XOAI21X1_56 gnd vdd _91_ _92_ _132_ _93_ OAI21X1
XAOI22X1_10 gnd vdd _132_ _131_ _133_ counter2.modo_1_bF$buf1 _101_ AOI22X1
XAOI21X1_20 gnd vdd _99_ _130_ _134_ _133_ AOI21X1
XNOR3X1_18 vdd gnd counter2.modo_1_bF$buf0 _112_ counter2.modo_2_bF$buf1 _135_ NOR3X1
XAOI21X1_21 gnd vdd _91_ _92_ _136_ _93_ AOI21X1
XNOR3X1_19 vdd gnd counter2.Q_clk<1> counter2.Q_clk<2> counter2.Q_clk<0> _137_ NOR3X1
XOAI21X1_57 gnd vdd _136_ _137_ _138_ _135_ OAI21X1
XNAND2X1_85 vdd _139_ gnd D<6> _119_ NAND2X1
XNAND2X1_86 vdd _140_ gnd _139_ _138_ NAND2X1
XOAI21X1_58 gnd vdd _140_ _134_ _141_ _122_ OAI21X1
XNAND2X1_87 vdd _142_ gnd _116_ _21_<6> NAND2X1
XNAND2X1_88 vdd _89_<2> gnd _142_ _141_ NAND2X1
XOAI21X1_59 gnd vdd _93_ _129_ _143_ _94_ OAI21X1
XNAND3X1_51 _143_ vdd gnd _98_ _97_ _144_ NAND3X1
XNAND2X1_89 vdd _145_ gnd D<7> _119_ NAND2X1
XNOR3X1_20 vdd gnd counter2.modo_0_bF$buf0 _106_ counter2.modo_2_bF$buf0 _146_ NOR3X1
XOAI21X1_60 gnd vdd counter2.Q_clk<2> _96_ _147_ _94_ OAI21X1
XNAND3X1_52 _93_ vdd gnd counter2.Q_clk<3> _129_ _148_ NAND3X1
XNAND3X1_53 _146_ vdd gnd _148_ _147_ _149_ NAND3X1
XNAND3X1_54 _145_ vdd gnd _144_ _149_ _150_ NAND3X1
XOAI21X1_61 gnd vdd _94_ _137_ _151_ _109_ OAI21X1
XAND2X2_12 vdd gnd _151_ _135_ _152_ AND2X2
XOAI21X1_62 gnd vdd _152_ _150_ _153_ _122_ OAI21X1
XNAND2X1_90 vdd _154_ gnd _116_ _21_<7> NAND2X1
XNAND2X1_91 vdd _89_<3> gnd _154_ _153_ NAND2X1
XDFFPOSX1_26 vdd _89_<0> gnd counter2.Q_clk<0> clk2 DFFPOSX1
XDFFPOSX1_27 vdd _89_<1> gnd counter2.Q_clk<1> clk2 DFFPOSX1
XDFFPOSX1_28 vdd _89_<2> gnd counter2.Q_clk<2> clk2 DFFPOSX1
XDFFPOSX1_29 vdd _89_<3> gnd counter2.Q_clk<3> clk2 DFFPOSX1
XDFFPOSX1_30 vdd _90_ gnd counter2.rco_clk clk2 DFFPOSX1
XINVX1_60 counter3.Q_clk<0> _157_ vdd gnd INVX1
XNOR2X1_69 vdd _157_ gnd _21_<8> reset_bF$buf4 NOR2X1
XINVX1_61 counter3.Q_clk<1> _158_ vdd gnd INVX1
XNOR2X1_70 vdd _158_ gnd _21_<9> reset_bF$buf3 NOR2X1
XINVX1_62 counter3.Q_clk<2> _159_ vdd gnd INVX1
XNOR2X1_71 vdd _159_ gnd _21_<10> reset_bF$buf2 NOR2X1
XINVX1_63 counter3.Q_clk<3> _160_ vdd gnd INVX1
XNOR2X1_72 vdd _160_ gnd _21_<11> reset_bF$buf1 NOR2X1
XINVX1_64 counter3.rco_clk _161_ vdd gnd INVX1
XNOR2X1_73 vdd _161_ gnd counter3.rco reset_bF$buf0 NOR2X1
XAND2X2_13 vdd gnd counter3.Q_clk<0> counter3.Q_clk<1> _162_ AND2X2
XNAND3X1_55 counter3.Q_clk<3> vdd gnd counter3.Q_clk<2> _162_ _163_ NAND3X1
XNOR3X1_21 vdd gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf5 counter2.modo_2_bF$buf6 _164_ NOR3X1
XINVX1_65 _164_ _165_ vdd gnd INVX1
XNOR2X1_74 vdd _165_ gnd _166_ _163_ NOR2X1
XNOR2X1_75 vdd counter2.modo_0_bF$buf4 gnd _167_ counter2.modo_2_bF$buf5 NOR2X1
XNAND2X1_92 vdd _168_ gnd counter2.modo_1_bF$buf4 _167_ NAND2X1
XNOR2X1_76 vdd counter3.Q_clk<3> gnd _169_ counter3.Q_clk<2> NOR2X1
XOAI21X1_63 gnd vdd _157_ _158_ _170_ _169_ OAI21X1
XINVX1_66 counter2.modo_2_bF$buf4 _171_ vdd gnd INVX1
XINVX1_67 counter2.modo_1_bF$buf3 _172_ vdd gnd INVX1
XNAND3X1_56 _171_ vdd gnd counter2.modo_0_bF$buf3 _172_ _173_ NAND3X1
XNOR2X1_77 vdd counter3.Q_clk<1> gnd _174_ counter3.Q_clk<0> NOR2X1
XNAND2X1_93 vdd _175_ gnd _169_ _174_ NAND2X1
XOAI22X1_6 gnd vdd _170_ _168_ _173_ _175_ _176_ OAI22X1
XOAI21X1_64 gnd vdd _176_ _166_ _177_ enable_bF$buf4 OAI21X1
XINVX1_68 counter2.modo_0_bF$buf2 _178_ vdd gnd INVX1
XNAND3X1_57 _178_ vdd gnd enable_bF$buf3 _172_ _179_ NAND3X1
XNAND2X1_94 vdd _180_ gnd enable_bF$buf2 _171_ NAND2X1
XNAND3X1_58 _180_ vdd gnd counter3.rco_clk _179_ _181_ NAND3X1
XAOI21X1_22 gnd vdd _177_ _181_ _156_ reset_bF$buf6 AOI21X1
XINVX1_69 enable_bF$buf1 _182_ vdd gnd INVX1
XNAND2X1_95 vdd _183_ gnd _182_ _21_<8> NAND2X1
XNAND2X1_96 vdd _184_ gnd counter2.modo_0_bF$buf1 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_78 vdd _184_ gnd _185_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_79 vdd counter2.modo_2_bF$buf2 gnd _186_ counter3.Q_clk<0> NOR2X1
XAOI22X1_11 gnd vdd _185_ D<8> _187_ _184_ _186_ AOI22X1
XNOR2X1_80 vdd _180_ gnd _188_ reset_bF$buf5 NOR2X1
XINVX1_70 _188_ _189_ vdd gnd INVX1
XOAI21X1_65 gnd vdd _187_ _189_ _155_<0> _183_ OAI21X1
XNOR2X1_81 vdd _162_ gnd _190_ _174_ NOR2X1
XNAND2X1_97 vdd _191_ gnd D<9> _185_ NAND2X1
XOAI21X1_66 gnd vdd _173_ _190_ _192_ _191_ OAI21X1
XAOI21X1_23 gnd vdd _167_ _190_ _193_ _192_ AOI21X1
XNAND2X1_98 vdd _194_ gnd _182_ _21_<9> NAND2X1
XOAI21X1_67 gnd vdd _189_ _193_ _155_<1> _194_ OAI21X1
XNAND2X1_99 vdd _195_ gnd counter3.Q_clk<0> counter3.Q_clk<1> NAND2X1
XXNOR2X1_6 _195_ counter3.Q_clk<2> gnd vdd _196_ XNOR2X1
XNAND3X1_59 counter3.Q_clk<1> vdd gnd counter3.Q_clk<0> counter3.Q_clk<2> _197_ NAND3X1
XOAI21X1_68 gnd vdd _157_ _158_ _198_ _159_ OAI21X1
XAOI22X1_12 gnd vdd _198_ _197_ _199_ counter2.modo_1_bF$buf1 _167_ AOI22X1
XAOI21X1_24 gnd vdd _165_ _196_ _200_ _199_ AOI21X1
XNOR3X1_22 vdd gnd counter2.modo_1_bF$buf0 _178_ counter2.modo_2_bF$buf1 _201_ NOR3X1
XAOI21X1_25 gnd vdd _157_ _158_ _202_ _159_ AOI21X1
XNOR3X1_23 vdd gnd counter3.Q_clk<1> counter3.Q_clk<2> counter3.Q_clk<0> _203_ NOR3X1
XOAI21X1_69 gnd vdd _202_ _203_ _204_ _201_ OAI21X1
XNAND2X1_100 vdd _205_ gnd D<10> _185_ NAND2X1
XNAND2X1_101 vdd _206_ gnd _205_ _204_ NAND2X1
XOAI21X1_70 gnd vdd _206_ _200_ _207_ _188_ OAI21X1
XNAND2X1_102 vdd _208_ gnd _182_ _21_<10> NAND2X1
XNAND2X1_103 vdd _155_<2> gnd _208_ _207_ NAND2X1
XOAI21X1_71 gnd vdd _159_ _195_ _209_ _160_ OAI21X1
XNAND3X1_60 _209_ vdd gnd _164_ _163_ _210_ NAND3X1
XNAND2X1_104 vdd _211_ gnd D<11> _185_ NAND2X1
XNOR3X1_24 vdd gnd counter2.modo_0_bF$buf0 _172_ counter2.modo_2_bF$buf0 _212_ NOR3X1
XOAI21X1_72 gnd vdd counter3.Q_clk<2> _162_ _213_ _160_ OAI21X1
XNAND3X1_61 _159_ vdd gnd counter3.Q_clk<3> _195_ _214_ NAND3X1
XNAND3X1_62 _212_ vdd gnd _214_ _213_ _215_ NAND3X1
XNAND3X1_63 _211_ vdd gnd _210_ _215_ _216_ NAND3X1
XOAI21X1_73 gnd vdd _160_ _203_ _217_ _175_ OAI21X1
XAND2X2_14 vdd gnd _217_ _201_ _218_ AND2X2
XOAI21X1_74 gnd vdd _218_ _216_ _219_ _188_ OAI21X1
XNAND2X1_105 vdd _220_ gnd _182_ _21_<11> NAND2X1
XNAND2X1_106 vdd _155_<3> gnd _220_ _219_ NAND2X1
XDFFPOSX1_31 vdd _155_<0> gnd counter3.Q_clk<0> clk3 DFFPOSX1
XDFFPOSX1_32 vdd _155_<1> gnd counter3.Q_clk<1> clk3 DFFPOSX1
XDFFPOSX1_33 vdd _155_<2> gnd counter3.Q_clk<2> clk3 DFFPOSX1
XDFFPOSX1_34 vdd _155_<3> gnd counter3.Q_clk<3> clk3 DFFPOSX1
XDFFPOSX1_35 vdd _156_ gnd counter3.rco_clk clk3 DFFPOSX1
XINVX1_71 counter4.Q_clk<0> _223_ vdd gnd INVX1
XNOR2X1_82 vdd _223_ gnd _21_<12> reset_bF$buf4 NOR2X1
XINVX1_72 counter4.Q_clk<1> _224_ vdd gnd INVX1
XNOR2X1_83 vdd _224_ gnd _21_<13> reset_bF$buf3 NOR2X1
XINVX1_73 counter4.Q_clk<2> _225_ vdd gnd INVX1
XNOR2X1_84 vdd _225_ gnd _21_<14> reset_bF$buf2 NOR2X1
XINVX1_74 counter4.Q_clk<3> _226_ vdd gnd INVX1
XNOR2X1_85 vdd _226_ gnd _21_<15> reset_bF$buf1 NOR2X1
XINVX1_75 counter4.rco_clk _227_ vdd gnd INVX1
XNOR2X1_86 vdd _227_ gnd counter4.rco reset_bF$buf0 NOR2X1
XAND2X2_15 vdd gnd counter4.Q_clk<0> counter4.Q_clk<1> _228_ AND2X2
XNAND3X1_64 counter4.Q_clk<3> vdd gnd counter4.Q_clk<2> _228_ _229_ NAND3X1
XNOR3X1_25 vdd gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf5 counter2.modo_2_bF$buf6 _230_ NOR3X1
XINVX1_76 _230_ _231_ vdd gnd INVX1
XNOR2X1_87 vdd _231_ gnd _232_ _229_ NOR2X1
XNOR2X1_88 vdd counter2.modo_0_bF$buf4 gnd _233_ counter2.modo_2_bF$buf5 NOR2X1
XNAND2X1_107 vdd _234_ gnd counter2.modo_1_bF$buf4 _233_ NAND2X1
XNOR2X1_89 vdd counter4.Q_clk<3> gnd _235_ counter4.Q_clk<2> NOR2X1
XOAI21X1_75 gnd vdd _223_ _224_ _236_ _235_ OAI21X1
XINVX1_77 counter2.modo_2_bF$buf4 _237_ vdd gnd INVX1
XINVX1_78 counter2.modo_1_bF$buf3 _238_ vdd gnd INVX1
XNAND3X1_65 _237_ vdd gnd counter2.modo_0_bF$buf3 _238_ _239_ NAND3X1
XNOR2X1_90 vdd counter4.Q_clk<1> gnd _240_ counter4.Q_clk<0> NOR2X1
XNAND2X1_108 vdd _241_ gnd _235_ _240_ NAND2X1
XOAI22X1_7 gnd vdd _236_ _234_ _239_ _241_ _242_ OAI22X1
XOAI21X1_76 gnd vdd _242_ _232_ _243_ enable_bF$buf0 OAI21X1
XINVX1_79 counter2.modo_0_bF$buf2 _244_ vdd gnd INVX1
XNAND3X1_66 _244_ vdd gnd enable_bF$buf4 _238_ _245_ NAND3X1
XNAND2X1_109 vdd _246_ gnd enable_bF$buf3 _237_ NAND2X1
XNAND3X1_67 _246_ vdd gnd counter4.rco_clk _245_ _247_ NAND3X1
XAOI21X1_26 gnd vdd _243_ _247_ _222_ reset_bF$buf6 AOI21X1
XINVX1_80 enable_bF$buf2 _248_ vdd gnd INVX1
XNAND2X1_110 vdd _249_ gnd _248_ _21_<12> NAND2X1
XNAND2X1_111 vdd _250_ gnd counter2.modo_0_bF$buf1 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_91 vdd _250_ gnd _251_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_92 vdd counter2.modo_2_bF$buf2 gnd _252_ counter4.Q_clk<0> NOR2X1
XAOI22X1_13 gnd vdd _251_ D<12> _253_ _250_ _252_ AOI22X1
XNOR2X1_93 vdd _246_ gnd _254_ reset_bF$buf5 NOR2X1
XINVX1_81 _254_ _255_ vdd gnd INVX1
XOAI21X1_77 gnd vdd _253_ _255_ _221_<0> _249_ OAI21X1
XNOR2X1_94 vdd _228_ gnd _256_ _240_ NOR2X1
XNAND2X1_112 vdd _257_ gnd D<13> _251_ NAND2X1
XOAI21X1_78 gnd vdd _239_ _256_ _258_ _257_ OAI21X1
XAOI21X1_27 gnd vdd _233_ _256_ _259_ _258_ AOI21X1
XNAND2X1_113 vdd _260_ gnd _248_ _21_<13> NAND2X1
XOAI21X1_79 gnd vdd _255_ _259_ _221_<1> _260_ OAI21X1
XNAND2X1_114 vdd _261_ gnd counter4.Q_clk<0> counter4.Q_clk<1> NAND2X1
XXNOR2X1_7 _261_ counter4.Q_clk<2> gnd vdd _262_ XNOR2X1
XNAND3X1_68 counter4.Q_clk<1> vdd gnd counter4.Q_clk<0> counter4.Q_clk<2> _263_ NAND3X1
XOAI21X1_80 gnd vdd _223_ _224_ _264_ _225_ OAI21X1
XAOI22X1_14 gnd vdd _264_ _263_ _265_ counter2.modo_1_bF$buf1 _233_ AOI22X1
XAOI21X1_28 gnd vdd _231_ _262_ _266_ _265_ AOI21X1
XNOR3X1_26 vdd gnd counter2.modo_1_bF$buf0 _244_ counter2.modo_2_bF$buf1 _267_ NOR3X1
XAOI21X1_29 gnd vdd _223_ _224_ _268_ _225_ AOI21X1
XNOR3X1_27 vdd gnd counter4.Q_clk<1> counter4.Q_clk<2> counter4.Q_clk<0> _269_ NOR3X1
XOAI21X1_81 gnd vdd _268_ _269_ _270_ _267_ OAI21X1
XNAND2X1_115 vdd _271_ gnd D<14> _251_ NAND2X1
XNAND2X1_116 vdd _272_ gnd _271_ _270_ NAND2X1
XOAI21X1_82 gnd vdd _272_ _266_ _273_ _254_ OAI21X1
XNAND2X1_117 vdd _274_ gnd _248_ _21_<14> NAND2X1
XNAND2X1_118 vdd _221_<2> gnd _274_ _273_ NAND2X1
XOAI21X1_83 gnd vdd _225_ _261_ _275_ _226_ OAI21X1
XNAND3X1_69 _275_ vdd gnd _230_ _229_ _276_ NAND3X1
XNAND2X1_119 vdd _277_ gnd D<15> _251_ NAND2X1
XNOR3X1_28 vdd gnd counter2.modo_0_bF$buf0 _238_ counter2.modo_2_bF$buf0 _278_ NOR3X1
XOAI21X1_84 gnd vdd counter4.Q_clk<2> _228_ _279_ _226_ OAI21X1
XNAND3X1_70 _225_ vdd gnd counter4.Q_clk<3> _261_ _280_ NAND3X1
XNAND3X1_71 _278_ vdd gnd _280_ _279_ _281_ NAND3X1
XNAND3X1_72 _277_ vdd gnd _276_ _281_ _282_ NAND3X1
XOAI21X1_85 gnd vdd _226_ _269_ _283_ _241_ OAI21X1
XAND2X2_16 vdd gnd _283_ _267_ _284_ AND2X2
XOAI21X1_86 gnd vdd _284_ _282_ _285_ _254_ OAI21X1
XNAND2X1_120 vdd _286_ gnd _248_ _21_<15> NAND2X1
XNAND2X1_121 vdd _221_<3> gnd _286_ _285_ NAND2X1
XDFFPOSX1_36 vdd _221_<0> gnd counter4.Q_clk<0> clk4 DFFPOSX1
XDFFPOSX1_37 vdd _221_<1> gnd counter4.Q_clk<1> clk4 DFFPOSX1
XDFFPOSX1_38 vdd _221_<2> gnd counter4.Q_clk<2> clk4 DFFPOSX1
XDFFPOSX1_39 vdd _221_<3> gnd counter4.Q_clk<3> clk4 DFFPOSX1
XDFFPOSX1_40 vdd _222_ gnd counter4.rco_clk clk4 DFFPOSX1
XINVX1_82 counter5.Q_clk<0> _289_ vdd gnd INVX1
XNOR2X1_95 vdd _289_ gnd _21_<16> reset_bF$buf4 NOR2X1
XINVX1_83 counter5.Q_clk<1> _290_ vdd gnd INVX1
XNOR2X1_96 vdd _290_ gnd _21_<17> reset_bF$buf3 NOR2X1
XINVX1_84 counter5.Q_clk<2> _291_ vdd gnd INVX1
XNOR2X1_97 vdd _291_ gnd _21_<18> reset_bF$buf2 NOR2X1
XINVX1_85 counter5.Q_clk<3> _292_ vdd gnd INVX1
XNOR2X1_98 vdd _292_ gnd _21_<19> reset_bF$buf1 NOR2X1
XINVX1_86 counter5.rco_clk _293_ vdd gnd INVX1
XNOR2X1_99 vdd _293_ gnd counter5.rco reset_bF$buf0 NOR2X1
XAND2X2_17 vdd gnd counter5.Q_clk<0> counter5.Q_clk<1> _294_ AND2X2
XNAND3X1_73 counter5.Q_clk<3> vdd gnd counter5.Q_clk<2> _294_ _295_ NAND3X1
XNOR3X1_29 vdd gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf5 counter2.modo_2_bF$buf6 _296_ NOR3X1
XINVX1_87 _296_ _297_ vdd gnd INVX1
XNOR2X1_100 vdd _297_ gnd _298_ _295_ NOR2X1
XNOR2X1_101 vdd counter2.modo_0_bF$buf4 gnd _299_ counter2.modo_2_bF$buf5 NOR2X1
XNAND2X1_122 vdd _300_ gnd counter2.modo_1_bF$buf4 _299_ NAND2X1
XNOR2X1_102 vdd counter5.Q_clk<3> gnd _301_ counter5.Q_clk<2> NOR2X1
XOAI21X1_87 gnd vdd _289_ _290_ _302_ _301_ OAI21X1
XINVX1_88 counter2.modo_2_bF$buf4 _303_ vdd gnd INVX1
XINVX1_89 counter2.modo_1_bF$buf3 _304_ vdd gnd INVX1
XNAND3X1_74 _303_ vdd gnd counter2.modo_0_bF$buf3 _304_ _305_ NAND3X1
XNOR2X1_103 vdd counter5.Q_clk<1> gnd _306_ counter5.Q_clk<0> NOR2X1
XNAND2X1_123 vdd _307_ gnd _301_ _306_ NAND2X1
XOAI22X1_8 gnd vdd _302_ _300_ _305_ _307_ _308_ OAI22X1
XOAI21X1_88 gnd vdd _308_ _298_ _309_ enable_bF$buf1 OAI21X1
XINVX1_90 counter2.modo_0_bF$buf2 _310_ vdd gnd INVX1
XNAND3X1_75 _310_ vdd gnd enable_bF$buf0 _304_ _311_ NAND3X1
XNAND2X1_124 vdd _312_ gnd enable_bF$buf4 _303_ NAND2X1
XNAND3X1_76 _312_ vdd gnd counter5.rco_clk _311_ _313_ NAND3X1
XAOI21X1_30 gnd vdd _309_ _313_ _288_ reset_bF$buf6 AOI21X1
XINVX1_91 enable_bF$buf3 _314_ vdd gnd INVX1
XNAND2X1_125 vdd _315_ gnd _314_ _21_<16> NAND2X1
XNAND2X1_126 vdd _316_ gnd counter2.modo_0_bF$buf1 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_104 vdd _316_ gnd _317_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_105 vdd counter2.modo_2_bF$buf2 gnd _318_ counter5.Q_clk<0> NOR2X1
XAOI22X1_15 gnd vdd _317_ D<16> _319_ _316_ _318_ AOI22X1
XNOR2X1_106 vdd _312_ gnd _320_ reset_bF$buf5 NOR2X1
XINVX1_92 _320_ _321_ vdd gnd INVX1
XOAI21X1_89 gnd vdd _319_ _321_ _287_<0> _315_ OAI21X1
XNOR2X1_107 vdd _294_ gnd _322_ _306_ NOR2X1
XNAND2X1_127 vdd _323_ gnd D<17> _317_ NAND2X1
XOAI21X1_90 gnd vdd _305_ _322_ _324_ _323_ OAI21X1
XAOI21X1_31 gnd vdd _299_ _322_ _325_ _324_ AOI21X1
XNAND2X1_128 vdd _326_ gnd _314_ _21_<17> NAND2X1
XOAI21X1_91 gnd vdd _321_ _325_ _287_<1> _326_ OAI21X1
XNAND2X1_129 vdd _327_ gnd counter5.Q_clk<0> counter5.Q_clk<1> NAND2X1
XXNOR2X1_8 _327_ counter5.Q_clk<2> gnd vdd _328_ XNOR2X1
XNAND3X1_77 counter5.Q_clk<1> vdd gnd counter5.Q_clk<0> counter5.Q_clk<2> _329_ NAND3X1
XOAI21X1_92 gnd vdd _289_ _290_ _330_ _291_ OAI21X1
XAOI22X1_16 gnd vdd _330_ _329_ _331_ counter2.modo_1_bF$buf1 _299_ AOI22X1
XAOI21X1_32 gnd vdd _297_ _328_ _332_ _331_ AOI21X1
XNOR3X1_30 vdd gnd counter2.modo_1_bF$buf0 _310_ counter2.modo_2_bF$buf1 _333_ NOR3X1
XAOI21X1_33 gnd vdd _289_ _290_ _334_ _291_ AOI21X1
XNOR3X1_31 vdd gnd counter5.Q_clk<1> counter5.Q_clk<2> counter5.Q_clk<0> _335_ NOR3X1
XOAI21X1_93 gnd vdd _334_ _335_ _336_ _333_ OAI21X1
XNAND2X1_130 vdd _337_ gnd D<18> _317_ NAND2X1
XNAND2X1_131 vdd _338_ gnd _337_ _336_ NAND2X1
XOAI21X1_94 gnd vdd _338_ _332_ _339_ _320_ OAI21X1
XNAND2X1_132 vdd _340_ gnd _314_ _21_<18> NAND2X1
XNAND2X1_133 vdd _287_<2> gnd _340_ _339_ NAND2X1
XOAI21X1_95 gnd vdd _291_ _327_ _341_ _292_ OAI21X1
XNAND3X1_78 _341_ vdd gnd _296_ _295_ _342_ NAND3X1
XNAND2X1_134 vdd _343_ gnd D<19> _317_ NAND2X1
XNOR3X1_32 vdd gnd counter2.modo_0_bF$buf0 _304_ counter2.modo_2_bF$buf0 _344_ NOR3X1
XOAI21X1_96 gnd vdd counter5.Q_clk<2> _294_ _345_ _292_ OAI21X1
XNAND3X1_79 _291_ vdd gnd counter5.Q_clk<3> _327_ _346_ NAND3X1
XNAND3X1_80 _344_ vdd gnd _346_ _345_ _347_ NAND3X1
.ends contador32_cond
 