*SPICE netlist created from BLIF module contador32_cond by blif2BSpice
.include /usr/share/qflow/tech/osu035/osu035_stdcells.sp
.subckt contador32_cond vdd gnd clk enable D<0> D<1> D<2> D<3> D<4> D<5> D<6> D<7> D<8> D<9> D<10> D<11> D<12> D<13> D<14> D<15> D<16> D<17> D<18> D<19> D<20> D<21> D<22> D<23> D<24> D<25> D<26> D<27> D<28> D<29> D<30> D<31> modo<0> modo<1> modo<2> reset Q<0> Q<1> Q<2> Q<3> Q<4> Q<5> Q<6> Q<7> Q<8> Q<9> Q<10> Q<11> Q<12> Q<13> Q<14> Q<15> Q<16> Q<17> Q<18> Q<19> Q<20> Q<21> Q<22> Q<23> Q<24> Q<25> Q<26> Q<27> Q<28> Q<29> Q<30> Q<31> rco 
XBUFX4_1 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf5 BUFX4
XBUFX4_2 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf4 BUFX4
XBUFX4_3 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf3 BUFX4
XBUFX4_4 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf2 BUFX4
XBUFX4_5 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf1 BUFX4
XBUFX2_1 vdd gnd counter2.modo<2> counter2.modo_2_bF$buf0 BUFX2
XBUFX4_6 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf3 BUFX4
XBUFX4_7 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf2 BUFX4
XBUFX2_2 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf1 BUFX2
XBUFX4_8 vdd gnd counter2.modo<1> counter2.modo_1_bF$buf0 BUFX4
XBUFX4_9 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf5 BUFX4
XBUFX4_10 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf4 BUFX4
XBUFX4_11 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf3 BUFX4
XBUFX4_12 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf2 BUFX4
XBUFX4_13 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf1 BUFX4
XBUFX4_14 vdd gnd counter2.modo<0> counter2.modo_0_bF$buf0 BUFX4
XBUFX4_15 vdd gnd enable enable_bF$buf5 BUFX4
XBUFX4_16 vdd gnd enable enable_bF$buf4 BUFX4
XBUFX4_17 vdd gnd enable enable_bF$buf3 BUFX4
XBUFX4_18 vdd gnd enable enable_bF$buf2 BUFX4
XBUFX4_19 vdd gnd enable enable_bF$buf1 BUFX4
XBUFX4_20 vdd gnd enable enable_bF$buf0 BUFX4
XBUFX4_21 vdd gnd reset reset_bF$buf6 BUFX4
XBUFX4_22 vdd gnd reset reset_bF$buf5 BUFX4
XBUFX4_23 vdd gnd reset reset_bF$buf4 BUFX4
XBUFX4_24 vdd gnd reset reset_bF$buf3 BUFX4
XBUFX4_25 vdd gnd reset reset_bF$buf2 BUFX4
XBUFX4_26 vdd gnd reset reset_bF$buf1 BUFX4
XBUFX4_27 vdd gnd reset reset_bF$buf0 BUFX4
XNAND2X1_1 vdd _364_ gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf3 NAND2X1
XNOR2X1_1 vdd _364_ gnd _365_ counter2.modo_2_bF$buf5 NOR2X1
XNOR2X1_2 vdd counter2.modo_2_bF$buf4 gnd _366_ counter6.Q_clk<0> NOR2X1
XAOI22X1_1 gnd vdd D<20> _365_ _367_ _364_ _366_ AOI22X1
XNAND2X1_2 vdd _368_ gnd enable_bF$buf5 _353_ NAND2X1
XNOR2X1_3 vdd _368_ gnd _369_ reset_bF$buf6 NOR2X1
XINVX1_1 _369_ _370_ vdd gnd INVX1
XOAI21X1_1 gnd vdd _367_ _370_ _336_<0> _363_ OAI21X1
XNOR2X1_4 vdd counter2.modo_0_bF$buf4 gnd _371_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_5 vdd _343_ gnd _372_ _355_ NOR2X1
XNAND2X1_3 vdd _373_ gnd D<21> _365_ NAND2X1
XOAI21X1_2 gnd vdd _354_ _372_ _374_ _373_ OAI21X1
XAOI21X1_1 gnd vdd _371_ _372_ _375_ _374_ AOI21X1
XNAND2X1_4 vdd _376_ gnd _362_ _19_<21> NAND2X1
XOAI21X1_3 gnd vdd _370_ _375_ _336_<1> _376_ OAI21X1
XNAND3X1_1 counter6.Q_clk<1> vdd gnd counter6.Q_clk<0> counter6.Q_clk<2> _377_ NAND3X1
XOAI21X1_4 gnd vdd _338_ _339_ _378_ _340_ OAI21X1
XAND2X2_1 vdd gnd _378_ _377_ _379_ AND2X2
XAOI21X1_2 gnd vdd _378_ _377_ _380_ _350_ AOI21X1
XAOI21X1_3 gnd vdd _379_ _346_ _381_ _380_ AOI21X1
XXNOR2X1_1 _355_ counter6.Q_clk<2> gnd vdd _382_ XNOR2X1
XNAND2X1_5 vdd _383_ gnd D<22> _365_ NAND2X1
XOAI21X1_5 gnd vdd _354_ _382_ _384_ _383_ OAI21X1
XOAI21X1_6 gnd vdd _384_ _381_ _385_ _369_ OAI21X1
XNAND2X1_6 vdd _386_ gnd _362_ _19_<22> NAND2X1
XNAND2X1_7 vdd _336_<2> gnd _386_ _385_ NAND2X1
XOAI21X1_7 gnd vdd _340_ _348_ _387_ _341_ OAI21X1
XNAND3X1_2 _387_ vdd gnd _345_ _344_ _388_ NAND3X1
XNAND2X1_8 vdd _389_ gnd D<23> _365_ NAND2X1
XOAI21X1_8 gnd vdd counter6.Q_clk<2> _343_ _390_ _341_ OAI21X1
XNAND3X1_3 counter6.Q_clk<3> vdd gnd _340_ _348_ _391_ NAND3X1
XNAND3X1_4 _391_ vdd gnd _350_ _390_ _392_ NAND3X1
XNAND3X1_5 _389_ vdd gnd _388_ _392_ _393_ NAND3X1
XNAND2X1_9 vdd _394_ gnd _338_ _339_ NAND2X1
XOAI21X1_9 gnd vdd counter6.Q_clk<2> _394_ _395_ counter6.Q_clk<3> OAI21X1
XAOI21X1_4 gnd vdd _395_ _356_ _396_ _354_ AOI21X1
XOAI21X1_10 gnd vdd _396_ _393_ _397_ _369_ OAI21X1
XNAND2X1_10 vdd _398_ gnd _362_ _19_<23> NAND2X1
XNAND2X1_11 vdd _336_<3> gnd _398_ _397_ NAND2X1
XDFFPOSX1_1 vdd _336_<0> gnd counter6.Q_clk<0> clk6 DFFPOSX1
XDFFPOSX1_2 vdd _336_<1> gnd counter6.Q_clk<1> clk6 DFFPOSX1
XDFFPOSX1_3 vdd _336_<2> gnd counter6.Q_clk<2> clk6 DFFPOSX1
XDFFPOSX1_4 vdd _336_<3> gnd counter6.Q_clk<3> clk6 DFFPOSX1
XDFFPOSX1_5 vdd _337_ gnd counter6.rco_clk clk6 DFFPOSX1
XINVX1_2 counter7.Q_clk<0> _401_ vdd gnd INVX1
XNOR2X1_6 vdd _401_ gnd _19_<24> reset_bF$buf5 NOR2X1
XINVX1_3 counter7.Q_clk<1> _402_ vdd gnd INVX1
XNOR2X1_7 vdd _402_ gnd _19_<25> reset_bF$buf4 NOR2X1
XINVX2_1 vdd gnd _403_ counter7.Q_clk<2> INVX2
XNOR2X1_8 vdd _403_ gnd _19_<26> reset_bF$buf3 NOR2X1
XINVX1_4 counter7.Q_clk<3> _404_ vdd gnd INVX1
XNOR2X1_9 vdd _404_ gnd _19_<27> reset_bF$buf2 NOR2X1
XINVX1_5 counter7.rco_clk _405_ vdd gnd INVX1
XNOR2X1_10 vdd _405_ gnd counter7.rco reset_bF$buf1 NOR2X1
XAND2X2_2 vdd gnd counter7.Q_clk<0> counter7.Q_clk<1> _406_ AND2X2
XNAND3X1_6 counter7.Q_clk<3> vdd gnd counter7.Q_clk<2> _406_ _407_ NAND3X1
XNOR3X1_1 vdd gnd counter2.modo_0_bF$buf3 counter2.modo_1_bF$buf2 counter2.modo_2_bF$buf2 _408_ NOR3X1
XINVX1_6 _408_ _409_ vdd gnd INVX1
XNOR2X1_11 vdd _409_ gnd _410_ _407_ NOR2X1
XNAND2X1_12 vdd _411_ gnd counter7.Q_clk<0> counter7.Q_clk<1> NAND2X1
XINVX1_7 counter2.modo_1_bF$buf1 _412_ vdd gnd INVX1
XNOR3X1_2 vdd gnd counter2.modo_0_bF$buf2 _412_ counter2.modo_2_bF$buf1 _413_ NOR3X1
XNOR2X1_12 vdd counter7.Q_clk<3> gnd _414_ counter7.Q_clk<2> NOR2X1
XNAND3X1_7 _414_ vdd gnd _411_ _413_ _415_ NAND3X1
XINVX1_8 counter2.modo_2_bF$buf0 _416_ vdd gnd INVX1
XNAND3X1_8 _416_ vdd gnd counter2.modo_0_bF$buf1 _412_ _417_ NAND3X1
XNOR2X1_13 vdd counter7.Q_clk<1> gnd _418_ counter7.Q_clk<0> NOR2X1
XNAND2X1_13 vdd _419_ gnd _414_ _418_ NAND2X1
XOAI21X1_11 gnd vdd _417_ _419_ _420_ _415_ OAI21X1
XOAI21X1_12 gnd vdd _410_ _420_ _421_ enable_bF$buf4 OAI21X1
XNAND2X1_14 vdd _422_ gnd enable_bF$buf3 _412_ NAND2X1
XAOI21X1_5 gnd vdd _416_ enable_bF$buf2 _423_ _405_ AOI21X1
XOAI21X1_13 gnd vdd counter2.modo_0_bF$buf0 _422_ _424_ _423_ OAI21X1
XAOI21X1_6 gnd vdd _421_ _424_ _400_ reset_bF$buf0 AOI21X1
XINVX1_9 enable_bF$buf1 _425_ vdd gnd INVX1
XNAND2X1_15 vdd _426_ gnd _425_ _19_<24> NAND2X1
XNAND2X1_16 vdd _427_ gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf0 NAND2X1
XNOR2X1_14 vdd _427_ gnd _428_ counter2.modo_2_bF$buf5 NOR2X1
XNOR2X1_15 vdd counter2.modo_2_bF$buf4 gnd _429_ counter7.Q_clk<0> NOR2X1
XAOI22X1_2 gnd vdd D<24> _428_ _430_ _427_ _429_ AOI22X1
XNAND2X1_17 vdd _431_ gnd enable_bF$buf0 _416_ NAND2X1
XNOR2X1_16 vdd _431_ gnd _432_ reset_bF$buf6 NOR2X1
XINVX1_10 _432_ _433_ vdd gnd INVX1
XOAI21X1_14 gnd vdd _430_ _433_ _399_<0> _426_ OAI21X1
XNOR2X1_17 vdd counter2.modo_0_bF$buf4 gnd _434_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_18 vdd _406_ gnd _435_ _418_ NOR2X1
XNAND2X1_18 vdd _436_ gnd D<25> _428_ NAND2X1
XOAI21X1_15 gnd vdd _417_ _435_ _437_ _436_ OAI21X1
XAOI21X1_7 gnd vdd _434_ _435_ _438_ _437_ AOI21X1
XNAND2X1_19 vdd _439_ gnd _425_ _19_<25> NAND2X1
XOAI21X1_16 gnd vdd _433_ _438_ _399_<1> _439_ OAI21X1
XNAND3X1_9 counter7.Q_clk<1> vdd gnd counter7.Q_clk<0> counter7.Q_clk<2> _440_ NAND3X1
XOAI21X1_17 gnd vdd _401_ _402_ _441_ _403_ OAI21X1
XAND2X2_3 vdd gnd _441_ _440_ _442_ AND2X2
XAOI21X1_8 gnd vdd _441_ _440_ _443_ _413_ AOI21X1
XAOI21X1_9 gnd vdd _442_ _409_ _444_ _443_ AOI21X1
XXNOR2X1_2 _418_ counter7.Q_clk<2> gnd vdd _445_ XNOR2X1
XNAND2X1_20 vdd _446_ gnd D<26> _428_ NAND2X1
XOAI21X1_18 gnd vdd _417_ _445_ _447_ _446_ OAI21X1
XOAI21X1_19 gnd vdd _447_ _444_ _448_ _432_ OAI21X1
XNAND2X1_21 vdd _449_ gnd _425_ _19_<26> NAND2X1
XNAND2X1_22 vdd _399_<2> gnd _449_ _448_ NAND2X1
XOAI21X1_20 gnd vdd _403_ _411_ _450_ _404_ OAI21X1
XNAND3X1_10 _450_ vdd gnd _408_ _407_ _451_ NAND3X1
XNAND2X1_23 vdd _452_ gnd D<27> _428_ NAND2X1
XOAI21X1_21 gnd vdd counter7.Q_clk<2> _406_ _453_ _404_ OAI21X1
XNAND3X1_11 counter7.Q_clk<3> vdd gnd _403_ _411_ _454_ NAND3X1
XNAND3X1_12 _454_ vdd gnd _413_ _453_ _455_ NAND3X1
XNAND3X1_13 _452_ vdd gnd _451_ _455_ _456_ NAND3X1
XNAND2X1_24 vdd _457_ gnd _401_ _402_ NAND2X1
XOAI21X1_22 gnd vdd counter7.Q_clk<2> _457_ _458_ counter7.Q_clk<3> OAI21X1
XAOI21X1_10 gnd vdd _458_ _419_ _459_ _417_ AOI21X1
XOAI21X1_23 gnd vdd _459_ _456_ _460_ _432_ OAI21X1
XNAND2X1_25 vdd _461_ gnd _425_ _19_<27> NAND2X1
XNAND2X1_26 vdd _399_<3> gnd _461_ _460_ NAND2X1
XDFFPOSX1_6 vdd _399_<0> gnd counter7.Q_clk<0> clk7 DFFPOSX1
XDFFPOSX1_7 vdd _399_<1> gnd counter7.Q_clk<1> clk7 DFFPOSX1
XDFFPOSX1_8 vdd _399_<2> gnd counter7.Q_clk<2> clk7 DFFPOSX1
XDFFPOSX1_9 vdd _399_<3> gnd counter7.Q_clk<3> clk7 DFFPOSX1
XDFFPOSX1_10 vdd _400_ gnd counter7.rco_clk clk7 DFFPOSX1
XINVX1_11 counter8.Q_clk<0> _464_ vdd gnd INVX1
XNOR2X1_19 vdd _464_ gnd _19_<28> reset_bF$buf5 NOR2X1
XINVX1_12 counter8.Q_clk<1> _465_ vdd gnd INVX1
XNOR2X1_20 vdd _465_ gnd _19_<29> reset_bF$buf4 NOR2X1
XINVX2_2 vdd gnd _466_ counter8.Q_clk<2> INVX2
XNOR2X1_21 vdd _466_ gnd _19_<30> reset_bF$buf3 NOR2X1
XINVX1_13 counter8.Q_clk<3> _467_ vdd gnd INVX1
XNOR2X1_22 vdd _467_ gnd _19_<31> reset_bF$buf2 NOR2X1
XINVX1_14 counter8.rco_clk _468_ vdd gnd INVX1
XNOR2X1_23 vdd _468_ gnd counter8.rco reset_bF$buf1 NOR2X1
XAND2X2_4 vdd gnd counter8.Q_clk<0> counter8.Q_clk<1> _469_ AND2X2
XNAND3X1_14 counter8.Q_clk<3> vdd gnd counter8.Q_clk<2> _469_ _470_ NAND3X1
XNOR3X1_3 vdd gnd counter2.modo_0_bF$buf3 counter2.modo_1_bF$buf3 counter2.modo_2_bF$buf2 _471_ NOR3X1
XINVX1_15 _471_ _472_ vdd gnd INVX1
XNOR2X1_24 vdd _472_ gnd _473_ _470_ NOR2X1
XNAND2X1_27 vdd _474_ gnd counter8.Q_clk<0> counter8.Q_clk<1> NAND2X1
XINVX1_16 counter2.modo_1_bF$buf2 _475_ vdd gnd INVX1
XNOR3X1_4 vdd gnd counter2.modo_0_bF$buf2 _475_ counter2.modo_2_bF$buf1 _476_ NOR3X1
XNOR2X1_25 vdd counter8.Q_clk<3> gnd _477_ counter8.Q_clk<2> NOR2X1
XNAND3X1_15 _477_ vdd gnd _474_ _476_ _478_ NAND3X1
XINVX1_17 counter2.modo_2_bF$buf0 _479_ vdd gnd INVX1
XNAND3X1_16 _479_ vdd gnd counter2.modo_0_bF$buf1 _475_ _480_ NAND3X1
XNOR2X1_26 vdd counter8.Q_clk<1> gnd _481_ counter8.Q_clk<0> NOR2X1
XNAND2X1_28 vdd _482_ gnd _477_ _481_ NAND2X1
XOAI21X1_24 gnd vdd _480_ _482_ _483_ _478_ OAI21X1
XOAI21X1_25 gnd vdd _473_ _483_ _484_ enable_bF$buf5 OAI21X1
XNAND2X1_29 vdd _485_ gnd enable_bF$buf4 _475_ NAND2X1
XAOI21X1_11 gnd vdd _479_ enable_bF$buf3 _486_ _468_ AOI21X1
XOAI21X1_26 gnd vdd counter2.modo_0_bF$buf0 _485_ _487_ _486_ OAI21X1
XAOI21X1_12 gnd vdd _484_ _487_ _463_ reset_bF$buf0 AOI21X1
XINVX1_18 enable_bF$buf2 _488_ vdd gnd INVX1
XNAND2X1_30 vdd _489_ gnd _488_ _19_<28> NAND2X1
XNAND2X1_31 vdd _490_ gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf1 NAND2X1
XNOR2X1_27 vdd _490_ gnd _491_ counter2.modo_2_bF$buf5 NOR2X1
XNOR2X1_28 vdd counter2.modo_2_bF$buf4 gnd _492_ counter8.Q_clk<0> NOR2X1
XAOI22X1_3 gnd vdd D<28> _491_ _493_ _490_ _492_ AOI22X1
XNAND2X1_32 vdd _494_ gnd enable_bF$buf1 _479_ NAND2X1
XNOR2X1_29 vdd _494_ gnd _495_ reset_bF$buf6 NOR2X1
XINVX1_19 _495_ _496_ vdd gnd INVX1
XOAI21X1_27 gnd vdd _493_ _496_ _462_<0> _489_ OAI21X1
XNOR2X1_30 vdd counter2.modo_0_bF$buf4 gnd _497_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_31 vdd _469_ gnd _498_ _481_ NOR2X1
XNAND2X1_33 vdd _499_ gnd D<29> _491_ NAND2X1
XOAI21X1_28 gnd vdd _480_ _498_ _500_ _499_ OAI21X1
XAOI21X1_13 gnd vdd _497_ _498_ _501_ _500_ AOI21X1
XNAND2X1_34 vdd _502_ gnd _488_ _19_<29> NAND2X1
XOAI21X1_29 gnd vdd _496_ _501_ _462_<1> _502_ OAI21X1
XNAND3X1_17 counter8.Q_clk<1> vdd gnd counter8.Q_clk<0> counter8.Q_clk<2> _503_ NAND3X1
XOAI21X1_30 gnd vdd _464_ _465_ _504_ _466_ OAI21X1
XAND2X2_5 vdd gnd _504_ _503_ _505_ AND2X2
XAOI21X1_14 gnd vdd _504_ _503_ _506_ _476_ AOI21X1
XAOI21X1_15 gnd vdd _505_ _472_ _507_ _506_ AOI21X1
XXNOR2X1_3 _481_ counter8.Q_clk<2> gnd vdd _508_ XNOR2X1
XNAND2X1_35 vdd _509_ gnd D<30> _491_ NAND2X1
XOAI21X1_31 gnd vdd _480_ _508_ _510_ _509_ OAI21X1
XOAI21X1_32 gnd vdd _510_ _507_ _511_ _495_ OAI21X1
XNAND2X1_36 vdd _512_ gnd _488_ _19_<30> NAND2X1
XNAND2X1_37 vdd _462_<2> gnd _512_ _511_ NAND2X1
XOAI21X1_33 gnd vdd _466_ _474_ _513_ _467_ OAI21X1
XNAND3X1_18 _513_ vdd gnd _471_ _470_ _514_ NAND3X1
XNAND2X1_38 vdd _515_ gnd D<31> _491_ NAND2X1
XOAI21X1_34 gnd vdd counter8.Q_clk<2> _469_ _516_ _467_ OAI21X1
XNAND3X1_19 counter8.Q_clk<3> vdd gnd _466_ _474_ _517_ NAND3X1
XNAND3X1_20 _517_ vdd gnd _476_ _516_ _518_ NAND3X1
XNAND3X1_21 _515_ vdd gnd _514_ _518_ _519_ NAND3X1
XNAND2X1_39 vdd _520_ gnd _464_ _465_ NAND2X1
XOAI21X1_35 gnd vdd counter8.Q_clk<2> _520_ _521_ counter8.Q_clk<3> OAI21X1
XAOI21X1_16 gnd vdd _521_ _482_ _522_ _480_ AOI21X1
XOAI21X1_36 gnd vdd _522_ _519_ _523_ _495_ OAI21X1
XNAND2X1_40 vdd _524_ gnd _488_ _19_<31> NAND2X1
XNAND2X1_41 vdd _462_<3> gnd _524_ _523_ NAND2X1
XDFFPOSX1_11 vdd _462_<0> gnd counter8.Q_clk<0> clk8 DFFPOSX1
XDFFPOSX1_12 vdd _462_<1> gnd counter8.Q_clk<1> clk8 DFFPOSX1
XDFFPOSX1_13 vdd _462_<2> gnd counter8.Q_clk<2> clk8 DFFPOSX1
XDFFPOSX1_14 vdd _462_<3> gnd counter8.Q_clk<3> clk8 DFFPOSX1
XDFFPOSX1_15 vdd _463_ gnd counter8.rco_clk clk8 DFFPOSX1
XOR2X2_1 counter2.modo<0> modo<0> vdd gnd modo<1> OR2X2
XAND2X2_6 vdd gnd modo<1> modo<0> counter2.modo<1> AND2X2
XINVX1_20 modo<0> _0_ vdd gnd INVX1
XINVX1_21 modo<2> _1_ vdd gnd INVX1
XAOI21X1_17 gnd vdd _0_ modo<1> counter2.modo<2> _1_ AOI21X1
XNAND2X1_42 vdd _2_ gnd modo<1> modo<0> NAND2X1
XNOR2X1_32 vdd reset_bF$buf5 gnd _3_ modo<2> NOR2X1
XNAND3X1_22 _2_ vdd gnd counter1.rco _3_ _4_ NAND3X1
XOR2X2_2 _5_ reset_bF$buf4 vdd gnd modo<2> OR2X2
XOAI21X1_37 gnd vdd counter2.modo_1_bF$buf0 _5_ _6_ clk OAI21X1
XNAND2X1_43 vdd clk2 gnd _4_ _6_ NAND2X1
XNAND3X1_23 _2_ vdd gnd counter2.rco _3_ _7_ NAND3X1
XNAND2X1_44 vdd clk3 gnd _7_ _6_ NAND2X1
XNAND3X1_24 _2_ vdd gnd counter3.rco _3_ _8_ NAND3X1
XNAND2X1_45 vdd clk4 gnd _8_ _6_ NAND2X1
XNAND3X1_25 _2_ vdd gnd counter4.rco _3_ _9_ NAND3X1
XNAND2X1_46 vdd clk5 gnd _9_ _6_ NAND2X1
XNAND3X1_26 _2_ vdd gnd counter5.rco _3_ _10_ NAND3X1
XNAND2X1_47 vdd clk6 gnd _10_ _6_ NAND2X1
XNAND3X1_27 _2_ vdd gnd counter6.rco _3_ _11_ NAND3X1
XNAND2X1_48 vdd clk7 gnd _11_ _6_ NAND2X1
XNAND3X1_28 _2_ vdd gnd counter7.rco _3_ _12_ NAND3X1
XNAND2X1_49 vdd clk8 gnd _12_ _6_ NAND2X1
XNAND2X1_50 vdd _13_ gnd counter5.rco counter6.rco NAND2X1
XNAND2X1_51 vdd _14_ gnd counter7.rco counter8.rco NAND2X1
XNOR2X1_33 vdd _14_ gnd _15_ _13_ NOR2X1
XNAND2X1_52 vdd _16_ gnd counter1.rco counter2.rco NAND2X1
XNAND2X1_53 vdd _17_ gnd counter3.rco counter4.rco NAND2X1
XNOR2X1_34 vdd _17_ gnd _18_ _16_ NOR2X1
XAND2X2_7 vdd gnd _15_ _18_ _20_ AND2X2
XBUFX2_3 vdd gnd _19_<0> Q<0> BUFX2
XBUFX2_4 vdd gnd _19_<1> Q<1> BUFX2
XBUFX2_5 vdd gnd _19_<2> Q<2> BUFX2
XBUFX2_6 vdd gnd _19_<3> Q<3> BUFX2
XBUFX2_7 vdd gnd _19_<4> Q<4> BUFX2
XBUFX2_8 vdd gnd _19_<5> Q<5> BUFX2
XBUFX2_9 vdd gnd _19_<6> Q<6> BUFX2
XBUFX2_10 vdd gnd _19_<7> Q<7> BUFX2
XBUFX2_11 vdd gnd _19_<8> Q<8> BUFX2
XBUFX2_12 vdd gnd _19_<9> Q<9> BUFX2
XBUFX2_13 vdd gnd _19_<10> Q<10> BUFX2
XBUFX2_14 vdd gnd _19_<11> Q<11> BUFX2
XBUFX2_15 vdd gnd _19_<12> Q<12> BUFX2
XBUFX2_16 vdd gnd _19_<13> Q<13> BUFX2
XBUFX2_17 vdd gnd _19_<14> Q<14> BUFX2
XBUFX2_18 vdd gnd _19_<15> Q<15> BUFX2
XBUFX2_19 vdd gnd _19_<16> Q<16> BUFX2
XBUFX2_20 vdd gnd _19_<17> Q<17> BUFX2
XBUFX2_21 vdd gnd _19_<18> Q<18> BUFX2
XBUFX2_22 vdd gnd _19_<19> Q<19> BUFX2
XBUFX2_23 vdd gnd _19_<20> Q<20> BUFX2
XBUFX2_24 vdd gnd _19_<21> Q<21> BUFX2
XBUFX2_25 vdd gnd _19_<22> Q<22> BUFX2
XBUFX2_26 vdd gnd _19_<23> Q<23> BUFX2
XBUFX2_27 vdd gnd _19_<24> Q<24> BUFX2
XBUFX2_28 vdd gnd _19_<25> Q<25> BUFX2
XBUFX2_29 vdd gnd _19_<26> Q<26> BUFX2
XBUFX2_30 vdd gnd _19_<27> Q<27> BUFX2
XBUFX2_31 vdd gnd _19_<28> Q<28> BUFX2
XBUFX2_32 vdd gnd _19_<29> Q<29> BUFX2
XBUFX2_33 vdd gnd _19_<30> Q<30> BUFX2
XBUFX2_34 vdd gnd _19_<31> Q<31> BUFX2
XBUFX2_35 vdd gnd _20_ rco BUFX2
XINVX1_22 counter1.Q_clk<0> _23_ vdd gnd INVX1
XNOR2X1_35 vdd _23_ gnd _19_<0> reset_bF$buf3 NOR2X1
XINVX1_23 counter1.Q_clk<1> _24_ vdd gnd INVX1
XNOR2X1_36 vdd _24_ gnd _19_<1> reset_bF$buf2 NOR2X1
XINVX2_3 vdd gnd _25_ counter1.Q_clk<2> INVX2
XNOR2X1_37 vdd _25_ gnd _19_<2> reset_bF$buf1 NOR2X1
XINVX1_24 counter1.Q_clk<3> _26_ vdd gnd INVX1
XNOR2X1_38 vdd _26_ gnd _19_<3> reset_bF$buf0 NOR2X1
XINVX1_25 counter1.rco_clk _27_ vdd gnd INVX1
XNOR2X1_39 vdd _27_ gnd counter1.rco reset_bF$buf6 NOR2X1
XAND2X2_8 vdd gnd counter1.Q_clk<0> counter1.Q_clk<1> _28_ AND2X2
XNAND3X1_29 counter1.Q_clk<3> vdd gnd counter1.Q_clk<2> _28_ _29_ NAND3X1
XNOR3X1_5 vdd gnd modo<0> modo<1> modo<2> _30_ NOR3X1
XINVX1_26 _30_ _31_ vdd gnd INVX1
XNOR2X1_40 vdd _31_ gnd _32_ _29_ NOR2X1
XNAND2X1_54 vdd _33_ gnd counter1.Q_clk<0> counter1.Q_clk<1> NAND2X1
XINVX1_27 modo<1> _34_ vdd gnd INVX1
XNOR3X1_6 vdd gnd modo<0> _34_ modo<2> _35_ NOR3X1
XNOR2X1_41 vdd counter1.Q_clk<3> gnd _36_ counter1.Q_clk<2> NOR2X1
XNAND3X1_30 _36_ vdd gnd _33_ _35_ _37_ NAND3X1
XINVX1_28 modo<2> _38_ vdd gnd INVX1
XNAND3X1_31 _38_ vdd gnd modo<0> _34_ _39_ NAND3X1
XNOR2X1_42 vdd counter1.Q_clk<1> gnd _40_ counter1.Q_clk<0> NOR2X1
XNAND2X1_55 vdd _41_ gnd _36_ _40_ NAND2X1
XOAI21X1_38 gnd vdd _39_ _41_ _42_ _37_ OAI21X1
XOAI21X1_39 gnd vdd _32_ _42_ _43_ enable_bF$buf0 OAI21X1
XNAND2X1_56 vdd _44_ gnd enable_bF$buf5 _34_ NAND2X1
XAOI21X1_18 gnd vdd _38_ enable_bF$buf4 _45_ _27_ AOI21X1
XOAI21X1_40 gnd vdd modo<0> _44_ _46_ _45_ OAI21X1
XAOI21X1_19 gnd vdd _43_ _46_ _22_ reset_bF$buf5 AOI21X1
XINVX1_29 enable_bF$buf3 _47_ vdd gnd INVX1
XNAND2X1_57 vdd _48_ gnd _47_ _19_<0> NAND2X1
XNAND2X1_58 vdd _49_ gnd modo<0> modo<1> NAND2X1
XNOR2X1_43 vdd _49_ gnd _50_ modo<2> NOR2X1
XNOR2X1_44 vdd modo<2> gnd _51_ counter1.Q_clk<0> NOR2X1
XAOI22X1_4 gnd vdd D<0> _50_ _52_ _49_ _51_ AOI22X1
XNAND2X1_59 vdd _53_ gnd enable_bF$buf2 _38_ NAND2X1
XNOR2X1_45 vdd _53_ gnd _54_ reset_bF$buf4 NOR2X1
XINVX1_30 _54_ _55_ vdd gnd INVX1
XOAI21X1_41 gnd vdd _52_ _55_ _21_<0> _48_ OAI21X1
XNOR2X1_46 vdd modo<0> gnd _56_ modo<2> NOR2X1
XNOR2X1_47 vdd _28_ gnd _57_ _40_ NOR2X1
XNAND2X1_60 vdd _58_ gnd D<1> _50_ NAND2X1
XOAI21X1_42 gnd vdd _39_ _57_ _59_ _58_ OAI21X1
XAOI21X1_20 gnd vdd _56_ _57_ _60_ _59_ AOI21X1
XNAND2X1_61 vdd _61_ gnd _47_ _19_<1> NAND2X1
XOAI21X1_43 gnd vdd _55_ _60_ _21_<1> _61_ OAI21X1
XNAND3X1_32 counter1.Q_clk<1> vdd gnd counter1.Q_clk<0> counter1.Q_clk<2> _62_ NAND3X1
XOAI21X1_44 gnd vdd _23_ _24_ _63_ _25_ OAI21X1
XAND2X2_9 vdd gnd _63_ _62_ _64_ AND2X2
XAOI21X1_21 gnd vdd _63_ _62_ _65_ _35_ AOI21X1
XAOI21X1_22 gnd vdd _64_ _31_ _66_ _65_ AOI21X1
XXNOR2X1_4 _40_ counter1.Q_clk<2> gnd vdd _67_ XNOR2X1
XNAND2X1_62 vdd _68_ gnd D<2> _50_ NAND2X1
XOAI21X1_45 gnd vdd _39_ _67_ _69_ _68_ OAI21X1
XOAI21X1_46 gnd vdd _69_ _66_ _70_ _54_ OAI21X1
XNAND2X1_63 vdd _71_ gnd _47_ _19_<2> NAND2X1
XNAND2X1_64 vdd _21_<2> gnd _71_ _70_ NAND2X1
XOAI21X1_47 gnd vdd _25_ _33_ _72_ _26_ OAI21X1
XNAND3X1_33 _72_ vdd gnd _30_ _29_ _73_ NAND3X1
XNAND2X1_65 vdd _74_ gnd D<3> _50_ NAND2X1
XOAI21X1_48 gnd vdd counter1.Q_clk<2> _28_ _75_ _26_ OAI21X1
XNAND3X1_34 counter1.Q_clk<3> vdd gnd _25_ _33_ _76_ NAND3X1
XNAND3X1_35 _76_ vdd gnd _35_ _75_ _77_ NAND3X1
XNAND3X1_36 _74_ vdd gnd _73_ _77_ _78_ NAND3X1
XNAND2X1_66 vdd _79_ gnd _23_ _24_ NAND2X1
XOAI21X1_49 gnd vdd counter1.Q_clk<2> _79_ _80_ counter1.Q_clk<3> OAI21X1
XAOI21X1_23 gnd vdd _80_ _41_ _81_ _39_ AOI21X1
XOAI21X1_50 gnd vdd _81_ _78_ _82_ _54_ OAI21X1
XNAND2X1_67 vdd _83_ gnd _47_ _19_<3> NAND2X1
XNAND2X1_68 vdd _21_<3> gnd _83_ _82_ NAND2X1
XDFFPOSX1_16 vdd _21_<0> gnd counter1.Q_clk<0> clk DFFPOSX1
XDFFPOSX1_17 vdd _21_<1> gnd counter1.Q_clk<1> clk DFFPOSX1
XDFFPOSX1_18 vdd _21_<2> gnd counter1.Q_clk<2> clk DFFPOSX1
XDFFPOSX1_19 vdd _21_<3> gnd counter1.Q_clk<3> clk DFFPOSX1
XDFFPOSX1_20 vdd _22_ gnd counter1.rco_clk clk DFFPOSX1
XINVX1_31 counter2.Q_clk<0> _86_ vdd gnd INVX1
XNOR2X1_48 vdd _86_ gnd _19_<4> reset_bF$buf3 NOR2X1
XINVX1_32 counter2.Q_clk<1> _87_ vdd gnd INVX1
XNOR2X1_49 vdd _87_ gnd _19_<5> reset_bF$buf2 NOR2X1
XINVX2_4 vdd gnd _88_ counter2.Q_clk<2> INVX2
XNOR2X1_50 vdd _88_ gnd _19_<6> reset_bF$buf1 NOR2X1
XINVX1_33 counter2.Q_clk<3> _89_ vdd gnd INVX1
XNOR2X1_51 vdd _89_ gnd _19_<7> reset_bF$buf0 NOR2X1
XINVX1_34 counter2.rco_clk _90_ vdd gnd INVX1
XNOR2X1_52 vdd _90_ gnd counter2.rco reset_bF$buf6 NOR2X1
XAND2X2_10 vdd gnd counter2.Q_clk<0> counter2.Q_clk<1> _91_ AND2X2
XNAND3X1_37 counter2.Q_clk<3> vdd gnd counter2.Q_clk<2> _91_ _92_ NAND3X1
XNOR3X1_7 vdd gnd counter2.modo_0_bF$buf3 counter2.modo_1_bF$buf3 counter2.modo_2_bF$buf2 _93_ NOR3X1
XINVX1_35 _93_ _94_ vdd gnd INVX1
XNOR2X1_53 vdd _94_ gnd _95_ _92_ NOR2X1
XNAND2X1_69 vdd _96_ gnd counter2.Q_clk<0> counter2.Q_clk<1> NAND2X1
XINVX1_36 counter2.modo_1_bF$buf2 _97_ vdd gnd INVX1
XNOR3X1_8 vdd gnd counter2.modo_0_bF$buf2 _97_ counter2.modo_2_bF$buf1 _98_ NOR3X1
XNOR2X1_54 vdd counter2.Q_clk<3> gnd _99_ counter2.Q_clk<2> NOR2X1
XNAND3X1_38 _99_ vdd gnd _96_ _98_ _100_ NAND3X1
XINVX1_37 counter2.modo_2_bF$buf0 _101_ vdd gnd INVX1
XNAND3X1_39 _101_ vdd gnd counter2.modo_0_bF$buf1 _97_ _102_ NAND3X1
XNOR2X1_55 vdd counter2.Q_clk<1> gnd _103_ counter2.Q_clk<0> NOR2X1
XNAND2X1_70 vdd _104_ gnd _99_ _103_ NAND2X1
XOAI21X1_51 gnd vdd _102_ _104_ _105_ _100_ OAI21X1
XOAI21X1_52 gnd vdd _95_ _105_ _106_ enable_bF$buf1 OAI21X1
XNAND2X1_71 vdd _107_ gnd enable_bF$buf0 _97_ NAND2X1
XAOI21X1_24 gnd vdd _101_ enable_bF$buf5 _108_ _90_ AOI21X1
XOAI21X1_53 gnd vdd counter2.modo_0_bF$buf0 _107_ _109_ _108_ OAI21X1
XAOI21X1_25 gnd vdd _106_ _109_ _85_ reset_bF$buf5 AOI21X1
XINVX1_38 enable_bF$buf4 _110_ vdd gnd INVX1
XNAND2X1_72 vdd _111_ gnd _110_ _19_<4> NAND2X1
XNAND2X1_73 vdd _112_ gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf1 NAND2X1
XNOR2X1_56 vdd _112_ gnd _113_ counter2.modo_2_bF$buf5 NOR2X1
XNOR2X1_57 vdd counter2.modo_2_bF$buf4 gnd _114_ counter2.Q_clk<0> NOR2X1
XAOI22X1_5 gnd vdd D<4> _113_ _115_ _112_ _114_ AOI22X1
XNAND2X1_74 vdd _116_ gnd enable_bF$buf3 _101_ NAND2X1
XNOR2X1_58 vdd _116_ gnd _117_ reset_bF$buf4 NOR2X1
XINVX1_39 _117_ _118_ vdd gnd INVX1
XOAI21X1_54 gnd vdd _115_ _118_ _84_<0> _111_ OAI21X1
XNOR2X1_59 vdd counter2.modo_0_bF$buf4 gnd _119_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_60 vdd _91_ gnd _120_ _103_ NOR2X1
XNAND2X1_75 vdd _121_ gnd D<5> _113_ NAND2X1
XOAI21X1_55 gnd vdd _102_ _120_ _122_ _121_ OAI21X1
XAOI21X1_26 gnd vdd _119_ _120_ _123_ _122_ AOI21X1
XNAND2X1_76 vdd _124_ gnd _110_ _19_<5> NAND2X1
XOAI21X1_56 gnd vdd _118_ _123_ _84_<1> _124_ OAI21X1
XNAND3X1_40 counter2.Q_clk<1> vdd gnd counter2.Q_clk<0> counter2.Q_clk<2> _125_ NAND3X1
XOAI21X1_57 gnd vdd _86_ _87_ _126_ _88_ OAI21X1
XAND2X2_11 vdd gnd _126_ _125_ _127_ AND2X2
XAOI21X1_27 gnd vdd _126_ _125_ _128_ _98_ AOI21X1
XAOI21X1_28 gnd vdd _127_ _94_ _129_ _128_ AOI21X1
XXNOR2X1_5 _103_ counter2.Q_clk<2> gnd vdd _130_ XNOR2X1
XNAND2X1_77 vdd _131_ gnd D<6> _113_ NAND2X1
XOAI21X1_58 gnd vdd _102_ _130_ _132_ _131_ OAI21X1
XOAI21X1_59 gnd vdd _132_ _129_ _133_ _117_ OAI21X1
XNAND2X1_78 vdd _134_ gnd _110_ _19_<6> NAND2X1
XNAND2X1_79 vdd _84_<2> gnd _134_ _133_ NAND2X1
XOAI21X1_60 gnd vdd _88_ _96_ _135_ _89_ OAI21X1
XNAND3X1_41 _135_ vdd gnd _93_ _92_ _136_ NAND3X1
XNAND2X1_80 vdd _137_ gnd D<7> _113_ NAND2X1
XOAI21X1_61 gnd vdd counter2.Q_clk<2> _91_ _138_ _89_ OAI21X1
XNAND3X1_42 counter2.Q_clk<3> vdd gnd _88_ _96_ _139_ NAND3X1
XNAND3X1_43 _139_ vdd gnd _98_ _138_ _140_ NAND3X1
XNAND3X1_44 _137_ vdd gnd _136_ _140_ _141_ NAND3X1
XNAND2X1_81 vdd _142_ gnd _86_ _87_ NAND2X1
XOAI21X1_62 gnd vdd counter2.Q_clk<2> _142_ _143_ counter2.Q_clk<3> OAI21X1
XAOI21X1_29 gnd vdd _143_ _104_ _144_ _102_ AOI21X1
XOAI21X1_63 gnd vdd _144_ _141_ _145_ _117_ OAI21X1
XNAND2X1_82 vdd _146_ gnd _110_ _19_<7> NAND2X1
XNAND2X1_83 vdd _84_<3> gnd _146_ _145_ NAND2X1
XDFFPOSX1_21 vdd _84_<0> gnd counter2.Q_clk<0> clk2 DFFPOSX1
XDFFPOSX1_22 vdd _84_<1> gnd counter2.Q_clk<1> clk2 DFFPOSX1
XDFFPOSX1_23 vdd _84_<2> gnd counter2.Q_clk<2> clk2 DFFPOSX1
XDFFPOSX1_24 vdd _84_<3> gnd counter2.Q_clk<3> clk2 DFFPOSX1
XDFFPOSX1_25 vdd _85_ gnd counter2.rco_clk clk2 DFFPOSX1
XINVX1_40 counter3.Q_clk<0> _149_ vdd gnd INVX1
XNOR2X1_61 vdd _149_ gnd _19_<8> reset_bF$buf3 NOR2X1
XINVX1_41 counter3.Q_clk<1> _150_ vdd gnd INVX1
XNOR2X1_62 vdd _150_ gnd _19_<9> reset_bF$buf2 NOR2X1
XINVX2_5 vdd gnd _151_ counter3.Q_clk<2> INVX2
XNOR2X1_63 vdd _151_ gnd _19_<10> reset_bF$buf1 NOR2X1
XINVX1_42 counter3.Q_clk<3> _152_ vdd gnd INVX1
XNOR2X1_64 vdd _152_ gnd _19_<11> reset_bF$buf0 NOR2X1
XINVX1_43 counter3.rco_clk _153_ vdd gnd INVX1
XNOR2X1_65 vdd _153_ gnd counter3.rco reset_bF$buf6 NOR2X1
XAND2X2_12 vdd gnd counter3.Q_clk<0> counter3.Q_clk<1> _154_ AND2X2
XNAND3X1_45 counter3.Q_clk<3> vdd gnd counter3.Q_clk<2> _154_ _155_ NAND3X1
XNOR3X1_9 vdd gnd counter2.modo_0_bF$buf3 counter2.modo_1_bF$buf0 counter2.modo_2_bF$buf2 _156_ NOR3X1
XINVX1_44 _156_ _157_ vdd gnd INVX1
XNOR2X1_66 vdd _157_ gnd _158_ _155_ NOR2X1
XNAND2X1_84 vdd _159_ gnd counter3.Q_clk<0> counter3.Q_clk<1> NAND2X1
XINVX1_45 counter2.modo_1_bF$buf3 _160_ vdd gnd INVX1
XNOR3X1_10 vdd gnd counter2.modo_0_bF$buf2 _160_ counter2.modo_2_bF$buf1 _161_ NOR3X1
XNOR2X1_67 vdd counter3.Q_clk<3> gnd _162_ counter3.Q_clk<2> NOR2X1
XNAND3X1_46 _162_ vdd gnd _159_ _161_ _163_ NAND3X1
XINVX1_46 counter2.modo_2_bF$buf0 _164_ vdd gnd INVX1
XNAND3X1_47 _164_ vdd gnd counter2.modo_0_bF$buf1 _160_ _165_ NAND3X1
XNOR2X1_68 vdd counter3.Q_clk<1> gnd _166_ counter3.Q_clk<0> NOR2X1
XNAND2X1_85 vdd _167_ gnd _162_ _166_ NAND2X1
XOAI21X1_64 gnd vdd _165_ _167_ _168_ _163_ OAI21X1
XOAI21X1_65 gnd vdd _158_ _168_ _169_ enable_bF$buf2 OAI21X1
XNAND2X1_86 vdd _170_ gnd enable_bF$buf1 _160_ NAND2X1
XAOI21X1_30 gnd vdd _164_ enable_bF$buf0 _171_ _153_ AOI21X1
XOAI21X1_66 gnd vdd counter2.modo_0_bF$buf0 _170_ _172_ _171_ OAI21X1
XAOI21X1_31 gnd vdd _169_ _172_ _148_ reset_bF$buf5 AOI21X1
XINVX1_47 enable_bF$buf5 _173_ vdd gnd INVX1
XNAND2X1_87 vdd _174_ gnd _173_ _19_<8> NAND2X1
XNAND2X1_88 vdd _175_ gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf2 NAND2X1
XNOR2X1_69 vdd _175_ gnd _176_ counter2.modo_2_bF$buf5 NOR2X1
XNOR2X1_70 vdd counter2.modo_2_bF$buf4 gnd _177_ counter3.Q_clk<0> NOR2X1
XAOI22X1_6 gnd vdd D<8> _176_ _178_ _175_ _177_ AOI22X1
XNAND2X1_89 vdd _179_ gnd enable_bF$buf4 _164_ NAND2X1
XNOR2X1_71 vdd _179_ gnd _180_ reset_bF$buf4 NOR2X1
XINVX1_48 _180_ _181_ vdd gnd INVX1
XOAI21X1_67 gnd vdd _178_ _181_ _147_<0> _174_ OAI21X1
XNOR2X1_72 vdd counter2.modo_0_bF$buf4 gnd _182_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_73 vdd _154_ gnd _183_ _166_ NOR2X1
XNAND2X1_90 vdd _184_ gnd D<9> _176_ NAND2X1
XOAI21X1_68 gnd vdd _165_ _183_ _185_ _184_ OAI21X1
XAOI21X1_32 gnd vdd _182_ _183_ _186_ _185_ AOI21X1
XNAND2X1_91 vdd _187_ gnd _173_ _19_<9> NAND2X1
XOAI21X1_69 gnd vdd _181_ _186_ _147_<1> _187_ OAI21X1
XNAND3X1_48 counter3.Q_clk<1> vdd gnd counter3.Q_clk<0> counter3.Q_clk<2> _188_ NAND3X1
XOAI21X1_70 gnd vdd _149_ _150_ _189_ _151_ OAI21X1
XAND2X2_13 vdd gnd _189_ _188_ _190_ AND2X2
XAOI21X1_33 gnd vdd _189_ _188_ _191_ _161_ AOI21X1
XAOI21X1_34 gnd vdd _190_ _157_ _192_ _191_ AOI21X1
XXNOR2X1_6 _166_ counter3.Q_clk<2> gnd vdd _193_ XNOR2X1
XNAND2X1_92 vdd _194_ gnd D<10> _176_ NAND2X1
XOAI21X1_71 gnd vdd _165_ _193_ _195_ _194_ OAI21X1
XOAI21X1_72 gnd vdd _195_ _192_ _196_ _180_ OAI21X1
XNAND2X1_93 vdd _197_ gnd _173_ _19_<10> NAND2X1
XNAND2X1_94 vdd _147_<2> gnd _197_ _196_ NAND2X1
XOAI21X1_73 gnd vdd _151_ _159_ _198_ _152_ OAI21X1
XNAND3X1_49 _198_ vdd gnd _156_ _155_ _199_ NAND3X1
XNAND2X1_95 vdd _200_ gnd D<11> _176_ NAND2X1
XOAI21X1_74 gnd vdd counter3.Q_clk<2> _154_ _201_ _152_ OAI21X1
XNAND3X1_50 counter3.Q_clk<3> vdd gnd _151_ _159_ _202_ NAND3X1
XNAND3X1_51 _202_ vdd gnd _161_ _201_ _203_ NAND3X1
XNAND3X1_52 _200_ vdd gnd _199_ _203_ _204_ NAND3X1
XNAND2X1_96 vdd _205_ gnd _149_ _150_ NAND2X1
XOAI21X1_75 gnd vdd counter3.Q_clk<2> _205_ _206_ counter3.Q_clk<3> OAI21X1
XAOI21X1_35 gnd vdd _206_ _167_ _207_ _165_ AOI21X1
XOAI21X1_76 gnd vdd _207_ _204_ _208_ _180_ OAI21X1
XNAND2X1_97 vdd _209_ gnd _173_ _19_<11> NAND2X1
XNAND2X1_98 vdd _147_<3> gnd _209_ _208_ NAND2X1
XDFFPOSX1_26 vdd _147_<0> gnd counter3.Q_clk<0> clk3 DFFPOSX1
XDFFPOSX1_27 vdd _147_<1> gnd counter3.Q_clk<1> clk3 DFFPOSX1
XDFFPOSX1_28 vdd _147_<2> gnd counter3.Q_clk<2> clk3 DFFPOSX1
XDFFPOSX1_29 vdd _147_<3> gnd counter3.Q_clk<3> clk3 DFFPOSX1
XDFFPOSX1_30 vdd _148_ gnd counter3.rco_clk clk3 DFFPOSX1
XINVX1_49 counter4.Q_clk<0> _212_ vdd gnd INVX1
XNOR2X1_74 vdd _212_ gnd _19_<12> reset_bF$buf3 NOR2X1
XINVX1_50 counter4.Q_clk<1> _213_ vdd gnd INVX1
XNOR2X1_75 vdd _213_ gnd _19_<13> reset_bF$buf2 NOR2X1
XINVX2_6 vdd gnd _214_ counter4.Q_clk<2> INVX2
XNOR2X1_76 vdd _214_ gnd _19_<14> reset_bF$buf1 NOR2X1
XINVX1_51 counter4.Q_clk<3> _215_ vdd gnd INVX1
XNOR2X1_77 vdd _215_ gnd _19_<15> reset_bF$buf0 NOR2X1
XINVX1_52 counter4.rco_clk _216_ vdd gnd INVX1
XNOR2X1_78 vdd _216_ gnd counter4.rco reset_bF$buf6 NOR2X1
XAND2X2_14 vdd gnd counter4.Q_clk<0> counter4.Q_clk<1> _217_ AND2X2
XNAND3X1_53 counter4.Q_clk<3> vdd gnd counter4.Q_clk<2> _217_ _218_ NAND3X1
XNOR3X1_11 vdd gnd counter2.modo_0_bF$buf3 counter2.modo_1_bF$buf1 counter2.modo_2_bF$buf2 _219_ NOR3X1
XINVX1_53 _219_ _220_ vdd gnd INVX1
XNOR2X1_79 vdd _220_ gnd _221_ _218_ NOR2X1
XNAND2X1_99 vdd _222_ gnd counter4.Q_clk<0> counter4.Q_clk<1> NAND2X1
XINVX1_54 counter2.modo_1_bF$buf0 _223_ vdd gnd INVX1
XNOR3X1_12 vdd gnd counter2.modo_0_bF$buf2 _223_ counter2.modo_2_bF$buf1 _224_ NOR3X1
XNOR2X1_80 vdd counter4.Q_clk<3> gnd _225_ counter4.Q_clk<2> NOR2X1
XNAND3X1_54 _225_ vdd gnd _222_ _224_ _226_ NAND3X1
XINVX1_55 counter2.modo_2_bF$buf0 _227_ vdd gnd INVX1
XNAND3X1_55 _227_ vdd gnd counter2.modo_0_bF$buf1 _223_ _228_ NAND3X1
XNOR2X1_81 vdd counter4.Q_clk<1> gnd _229_ counter4.Q_clk<0> NOR2X1
XNAND2X1_100 vdd _230_ gnd _225_ _229_ NAND2X1
XOAI21X1_77 gnd vdd _228_ _230_ _231_ _226_ OAI21X1
XOAI21X1_78 gnd vdd _221_ _231_ _232_ enable_bF$buf3 OAI21X1
XNAND2X1_101 vdd _233_ gnd enable_bF$buf2 _223_ NAND2X1
XAOI21X1_36 gnd vdd _227_ enable_bF$buf1 _234_ _216_ AOI21X1
XOAI21X1_79 gnd vdd counter2.modo_0_bF$buf0 _233_ _235_ _234_ OAI21X1
XAOI21X1_37 gnd vdd _232_ _235_ _211_ reset_bF$buf5 AOI21X1
XINVX1_56 enable_bF$buf0 _236_ vdd gnd INVX1
XNAND2X1_102 vdd _237_ gnd _236_ _19_<12> NAND2X1
XNAND2X1_103 vdd _238_ gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf3 NAND2X1
XNOR2X1_82 vdd _238_ gnd _239_ counter2.modo_2_bF$buf5 NOR2X1
XNOR2X1_83 vdd counter2.modo_2_bF$buf4 gnd _240_ counter4.Q_clk<0> NOR2X1
XAOI22X1_7 gnd vdd D<12> _239_ _241_ _238_ _240_ AOI22X1
XNAND2X1_104 vdd _242_ gnd enable_bF$buf5 _227_ NAND2X1
XNOR2X1_84 vdd _242_ gnd _243_ reset_bF$buf4 NOR2X1
XINVX1_57 _243_ _244_ vdd gnd INVX1
XOAI21X1_80 gnd vdd _241_ _244_ _210_<0> _237_ OAI21X1
XNOR2X1_85 vdd counter2.modo_0_bF$buf4 gnd _245_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_86 vdd _217_ gnd _246_ _229_ NOR2X1
XNAND2X1_105 vdd _247_ gnd D<13> _239_ NAND2X1
XOAI21X1_81 gnd vdd _228_ _246_ _248_ _247_ OAI21X1
XAOI21X1_38 gnd vdd _245_ _246_ _249_ _248_ AOI21X1
XNAND2X1_106 vdd _250_ gnd _236_ _19_<13> NAND2X1
XOAI21X1_82 gnd vdd _244_ _249_ _210_<1> _250_ OAI21X1
XNAND3X1_56 counter4.Q_clk<1> vdd gnd counter4.Q_clk<0> counter4.Q_clk<2> _251_ NAND3X1
XOAI21X1_83 gnd vdd _212_ _213_ _252_ _214_ OAI21X1
XAND2X2_15 vdd gnd _252_ _251_ _253_ AND2X2
XAOI21X1_39 gnd vdd _252_ _251_ _254_ _224_ AOI21X1
XAOI21X1_40 gnd vdd _253_ _220_ _255_ _254_ AOI21X1
XXNOR2X1_7 _229_ counter4.Q_clk<2> gnd vdd _256_ XNOR2X1
XNAND2X1_107 vdd _257_ gnd D<14> _239_ NAND2X1
XOAI21X1_84 gnd vdd _228_ _256_ _258_ _257_ OAI21X1
XOAI21X1_85 gnd vdd _258_ _255_ _259_ _243_ OAI21X1
XNAND2X1_108 vdd _260_ gnd _236_ _19_<14> NAND2X1
XNAND2X1_109 vdd _210_<2> gnd _260_ _259_ NAND2X1
XOAI21X1_86 gnd vdd _214_ _222_ _261_ _215_ OAI21X1
XNAND3X1_57 _261_ vdd gnd _219_ _218_ _262_ NAND3X1
XNAND2X1_110 vdd _263_ gnd D<15> _239_ NAND2X1
XOAI21X1_87 gnd vdd counter4.Q_clk<2> _217_ _264_ _215_ OAI21X1
XNAND3X1_58 counter4.Q_clk<3> vdd gnd _214_ _222_ _265_ NAND3X1
XNAND3X1_59 _265_ vdd gnd _224_ _264_ _266_ NAND3X1
XNAND3X1_60 _263_ vdd gnd _262_ _266_ _267_ NAND3X1
XNAND2X1_111 vdd _268_ gnd _212_ _213_ NAND2X1
XOAI21X1_88 gnd vdd counter4.Q_clk<2> _268_ _269_ counter4.Q_clk<3> OAI21X1
XAOI21X1_41 gnd vdd _269_ _230_ _270_ _228_ AOI21X1
XOAI21X1_89 gnd vdd _270_ _267_ _271_ _243_ OAI21X1
XNAND2X1_112 vdd _272_ gnd _236_ _19_<15> NAND2X1
XNAND2X1_113 vdd _210_<3> gnd _272_ _271_ NAND2X1
XDFFPOSX1_31 vdd _210_<0> gnd counter4.Q_clk<0> clk4 DFFPOSX1
XDFFPOSX1_32 vdd _210_<1> gnd counter4.Q_clk<1> clk4 DFFPOSX1
XDFFPOSX1_33 vdd _210_<2> gnd counter4.Q_clk<2> clk4 DFFPOSX1
XDFFPOSX1_34 vdd _210_<3> gnd counter4.Q_clk<3> clk4 DFFPOSX1
XDFFPOSX1_35 vdd _211_ gnd counter4.rco_clk clk4 DFFPOSX1
XINVX1_58 counter5.Q_clk<0> _275_ vdd gnd INVX1
XNOR2X1_87 vdd _275_ gnd _19_<16> reset_bF$buf3 NOR2X1
XINVX1_59 counter5.Q_clk<1> _276_ vdd gnd INVX1
XNOR2X1_88 vdd _276_ gnd _19_<17> reset_bF$buf2 NOR2X1
XINVX2_7 vdd gnd _277_ counter5.Q_clk<2> INVX2
XNOR2X1_89 vdd _277_ gnd _19_<18> reset_bF$buf1 NOR2X1
XINVX1_60 counter5.Q_clk<3> _278_ vdd gnd INVX1
XNOR2X1_90 vdd _278_ gnd _19_<19> reset_bF$buf0 NOR2X1
XINVX1_61 counter5.rco_clk _279_ vdd gnd INVX1
XNOR2X1_91 vdd _279_ gnd counter5.rco reset_bF$buf6 NOR2X1
XAND2X2_16 vdd gnd counter5.Q_clk<0> counter5.Q_clk<1> _280_ AND2X2
XNAND3X1_61 counter5.Q_clk<3> vdd gnd counter5.Q_clk<2> _280_ _281_ NAND3X1
XNOR3X1_13 vdd gnd counter2.modo_0_bF$buf3 counter2.modo_1_bF$buf2 counter2.modo_2_bF$buf2 _282_ NOR3X1
XINVX1_62 _282_ _283_ vdd gnd INVX1
XNOR2X1_92 vdd _283_ gnd _284_ _281_ NOR2X1
XNAND2X1_114 vdd _285_ gnd counter5.Q_clk<0> counter5.Q_clk<1> NAND2X1
XINVX1_63 counter2.modo_1_bF$buf1 _286_ vdd gnd INVX1
XNOR3X1_14 vdd gnd counter2.modo_0_bF$buf2 _286_ counter2.modo_2_bF$buf1 _287_ NOR3X1
XNOR2X1_93 vdd counter5.Q_clk<3> gnd _288_ counter5.Q_clk<2> NOR2X1
XNAND3X1_62 _288_ vdd gnd _285_ _287_ _289_ NAND3X1
XINVX1_64 counter2.modo_2_bF$buf0 _290_ vdd gnd INVX1
XNAND3X1_63 _290_ vdd gnd counter2.modo_0_bF$buf1 _286_ _291_ NAND3X1
XNOR2X1_94 vdd counter5.Q_clk<1> gnd _292_ counter5.Q_clk<0> NOR2X1
XNAND2X1_115 vdd _293_ gnd _288_ _292_ NAND2X1
XOAI21X1_90 gnd vdd _291_ _293_ _294_ _289_ OAI21X1
XOAI21X1_91 gnd vdd _284_ _294_ _295_ enable_bF$buf4 OAI21X1
XNAND2X1_116 vdd _296_ gnd enable_bF$buf3 _286_ NAND2X1
XAOI21X1_42 gnd vdd _290_ enable_bF$buf2 _297_ _279_ AOI21X1
XOAI21X1_92 gnd vdd counter2.modo_0_bF$buf0 _296_ _298_ _297_ OAI21X1
XAOI21X1_43 gnd vdd _295_ _298_ _274_ reset_bF$buf5 AOI21X1
XINVX1_65 enable_bF$buf1 _299_ vdd gnd INVX1
XNAND2X1_117 vdd _300_ gnd _299_ _19_<16> NAND2X1
XNAND2X1_118 vdd _301_ gnd counter2.modo_0_bF$buf5 counter2.modo_1_bF$buf0 NAND2X1
XNOR2X1_95 vdd _301_ gnd _302_ counter2.modo_2_bF$buf5 NOR2X1
XNOR2X1_96 vdd counter2.modo_2_bF$buf4 gnd _303_ counter5.Q_clk<0> NOR2X1
XAOI22X1_8 gnd vdd D<16> _302_ _304_ _301_ _303_ AOI22X1
XNAND2X1_119 vdd _305_ gnd enable_bF$buf0 _290_ NAND2X1
XNOR2X1_97 vdd _305_ gnd _306_ reset_bF$buf4 NOR2X1
XINVX1_66 _306_ _307_ vdd gnd INVX1
XOAI21X1_93 gnd vdd _304_ _307_ _273_<0> _300_ OAI21X1
XNOR2X1_98 vdd counter2.modo_0_bF$buf4 gnd _308_ counter2.modo_2_bF$buf3 NOR2X1
XNOR2X1_99 vdd _280_ gnd _309_ _292_ NOR2X1
XNAND2X1_120 vdd _310_ gnd D<17> _302_ NAND2X1
XOAI21X1_94 gnd vdd _291_ _309_ _311_ _310_ OAI21X1
XAOI21X1_44 gnd vdd _308_ _309_ _312_ _311_ AOI21X1
XNAND2X1_121 vdd _313_ gnd _299_ _19_<17> NAND2X1
XOAI21X1_95 gnd vdd _307_ _312_ _273_<1> _313_ OAI21X1
XNAND3X1_64 counter5.Q_clk<1> vdd gnd counter5.Q_clk<0> counter5.Q_clk<2> _314_ NAND3X1
XOAI21X1_96 gnd vdd _275_ _276_ _315_ _277_ OAI21X1
XAND2X2_17 vdd gnd _315_ _314_ _316_ AND2X2
XAOI21X1_45 gnd vdd _315_ _314_ _317_ _287_ AOI21X1
XAOI21X1_46 gnd vdd _316_ _283_ _318_ _317_ AOI21X1
XXNOR2X1_8 _292_ counter5.Q_clk<2> gnd vdd _319_ XNOR2X1
XNAND2X1_122 vdd _320_ gnd D<18> _302_ NAND2X1
XOAI21X1_97 gnd vdd _291_ _319_ _321_ _320_ OAI21X1
XOAI21X1_98 gnd vdd _321_ _318_ _322_ _306_ OAI21X1
XNAND2X1_123 vdd _323_ gnd _299_ _19_<18> NAND2X1
XNAND2X1_124 vdd _273_<2> gnd _323_ _322_ NAND2X1
XOAI21X1_99 gnd vdd _277_ _285_ _324_ _278_ OAI21X1
XNAND3X1_65 _324_ vdd gnd _282_ _281_ _325_ NAND3X1
XNAND2X1_125 vdd _326_ gnd D<19> _302_ NAND2X1
XOAI21X1_100 gnd vdd counter5.Q_clk<2> _280_ _327_ _278_ OAI21X1
XNAND3X1_66 counter5.Q_clk<3> vdd gnd _277_ _285_ _328_ NAND3X1
XNAND3X1_67 _328_ vdd gnd _287_ _327_ _329_ NAND3X1
XNAND3X1_68 _326_ vdd gnd _325_ _329_ _330_ NAND3X1
XNAND2X1_126 vdd _331_ gnd _275_ _276_ NAND2X1
XOAI21X1_101 gnd vdd counter5.Q_clk<2> _331_ _332_ counter5.Q_clk<3> OAI21X1
XAOI21X1_47 gnd vdd _332_ _293_ _333_ _291_ AOI21X1
XOAI21X1_102 gnd vdd _333_ _330_ _334_ _306_ OAI21X1
XNAND2X1_127 vdd _335_ gnd _299_ _19_<19> NAND2X1
XNAND2X1_128 vdd _273_<3> gnd _335_ _334_ NAND2X1
XDFFPOSX1_36 vdd _273_<0> gnd counter5.Q_clk<0> clk5 DFFPOSX1
XDFFPOSX1_37 vdd _273_<1> gnd counter5.Q_clk<1> clk5 DFFPOSX1
XDFFPOSX1_38 vdd _273_<2> gnd counter5.Q_clk<2> clk5 DFFPOSX1
XDFFPOSX1_39 vdd _273_<3> gnd counter5.Q_clk<3> clk5 DFFPOSX1
XDFFPOSX1_40 vdd _274_ gnd counter5.rco_clk clk5 DFFPOSX1
XINVX1_67 counter6.Q_clk<0> _338_ vdd gnd INVX1
XNOR2X1_100 vdd _338_ gnd _19_<20> reset_bF$buf3 NOR2X1
XINVX1_68 counter6.Q_clk<1> _339_ vdd gnd INVX1
XNOR2X1_101 vdd _339_ gnd _19_<21> reset_bF$buf2 NOR2X1
XINVX2_8 vdd gnd _340_ counter6.Q_clk<2> INVX2
XNOR2X1_102 vdd _340_ gnd _19_<22> reset_bF$buf1 NOR2X1
XINVX1_69 counter6.Q_clk<3> _341_ vdd gnd INVX1
XNOR2X1_103 vdd _341_ gnd _19_<23> reset_bF$buf0 NOR2X1
XINVX1_70 counter6.rco_clk _342_ vdd gnd INVX1
XNOR2X1_104 vdd _342_ gnd counter6.rco reset_bF$buf6 NOR2X1
XAND2X2_18 vdd gnd counter6.Q_clk<0> counter6.Q_clk<1> _343_ AND2X2
XNAND3X1_69 counter6.Q_clk<3> vdd gnd counter6.Q_clk<2> _343_ _344_ NAND3X1
XNOR3X1_15 vdd gnd counter2.modo_0_bF$buf3 counter2.modo_1_bF$buf3 counter2.modo_2_bF$buf2 _345_ NOR3X1
XINVX1_71 _345_ _346_ vdd gnd INVX1
XNOR2X1_105 vdd _346_ gnd _347_ _344_ NOR2X1
XNAND2X1_129 vdd _348_ gnd counter6.Q_clk<0> counter6.Q_clk<1> NAND2X1
XINVX1_72 counter2.modo_1_bF$buf2 _349_ vdd gnd INVX1
XNOR3X1_16 vdd gnd counter2.modo_0_bF$buf2 _349_ counter2.modo_2_bF$buf1 _350_ NOR3X1
XNOR2X1_106 vdd counter6.Q_clk<3> gnd _351_ counter6.Q_clk<2> NOR2X1
XNAND3X1_70 _351_ vdd gnd _348_ _350_ _352_ NAND3X1
XINVX1_73 counter2.modo_2_bF$buf0 _353_ vdd gnd INVX1
XNAND3X1_71 _353_ vdd gnd counter2.modo_0_bF$buf1 _349_ _354_ NAND3X1
XNOR2X1_107 vdd counter6.Q_clk<1> gnd _355_ counter6.Q_clk<0> NOR2X1
XNAND2X1_130 vdd _356_ gnd _351_ _355_ NAND2X1
XOAI21X1_103 gnd vdd _354_ _356_ _357_ _352_ OAI21X1
XOAI21X1_104 gnd vdd _347_ _357_ _358_ enable_bF$buf5 OAI21X1
XNAND2X1_131 vdd _359_ gnd enable_bF$buf4 _349_ NAND2X1
XAOI21X1_48 gnd vdd _353_ enable_bF$buf3 _360_ _342_ AOI21X1
XOAI21X1_105 gnd vdd counter2.modo_0_bF$buf0 _359_ _361_ _360_ OAI21X1
XAOI21X1_49 gnd vdd _358_ _361_ _337_ reset_bF$buf5 AOI21X1
XINVX1_74 enable_bF$buf2 _362_ vdd gnd INVX1
XNAND2X1_132 vdd _363_ gnd _362_ _19_<20> NAND2X1
.ends contador32_cond
 