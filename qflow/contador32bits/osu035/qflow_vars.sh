#!/usr/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/fabian/Documentos/microe/proyecto/qflow/contador32bits/osu035
#-------------------------------------------

set projectpath=/home/fabian/Documentos/microe/proyecto/qflow/contador32bits/osu035
set techdir=/usr/share/qflow/tech/osu035
set sourcedir=/home/fabian/Documentos/microe/proyecto/qflow/contador32bits/osu035/source
set synthdir=/home/fabian/Documentos/microe/proyecto/qflow/contador32bits/osu035/synthesis
set layoutdir=/home/fabian/Documentos/microe/proyecto/qflow/contador32bits/osu035/layout
set techname=osu035
set scriptdir=/usr/lib/qflow/scripts
set bindir=/usr/lib/qflow/bin
set synthlog=/home/fabian/Documentos/microe/proyecto/qflow/contador32bits/osu035/synth.log
#-------------------------------------------

