*SPICE netlist created from BLIF module contador_cond by blif2BSpice
.include /usr/share/qflow/tech/osu035/osu035_stdcells.sp
.subckt contador_cond vdd gnd clk enable D<0> D<1> D<2> D<3> modo<0> modo<1> modo<2> reset Q<0> Q<1> Q<2> Q<3> rco 
XAOI22X1_1 gnd vdd D<0> _29_ _31_ _28_ _30_ AOI22X1
XNAND2X1_1 vdd _32_ gnd enable _17_ NAND2X1
XNOR2X1_1 vdd _32_ gnd _33_ reset NOR2X1
XINVX1_1 _33_ _34_ vdd gnd INVX1
XOAI21X1_1 gnd vdd _31_ _34_ _0_<0> _27_ OAI21X1
XNOR2X1_2 vdd modo<0> gnd _35_ modo<2> NOR2X1
XNOR2X1_3 vdd _7_ gnd _36_ _19_ NOR2X1
XNAND2X1_2 vdd _37_ gnd D<1> _29_ NAND2X1
XOAI21X1_2 gnd vdd _18_ _36_ _38_ _37_ OAI21X1
XAOI21X1_1 gnd vdd _35_ _36_ _39_ _38_ AOI21X1
XNAND2X1_3 vdd _40_ gnd _26_ _63_<1> NAND2X1
XOAI21X1_3 gnd vdd _34_ _39_ _0_<1> _40_ OAI21X1
XNAND3X1_1 Q_clk<1> vdd gnd Q_clk<0> Q_clk<2> _41_ NAND3X1
XOAI21X1_4 gnd vdd _3_ _4_ _42_ _5_ OAI21X1
XAND2X2_1 vdd gnd _42_ _41_ _43_ AND2X2
XAOI21X1_2 gnd vdd _42_ _41_ _44_ _14_ AOI21X1
XAOI21X1_3 gnd vdd _43_ _10_ _45_ _44_ AOI21X1
XXNOR2X1_1 _19_ Q_clk<2> gnd vdd _46_ XNOR2X1
XNAND2X1_4 vdd _47_ gnd D<2> _29_ NAND2X1
XOAI21X1_5 gnd vdd _18_ _46_ _48_ _47_ OAI21X1
XOAI21X1_6 gnd vdd _48_ _45_ _49_ _33_ OAI21X1
XNAND2X1_5 vdd _50_ gnd _26_ _63_<2> NAND2X1
XNAND2X1_6 vdd _0_<2> gnd _50_ _49_ NAND2X1
XOAI21X1_7 gnd vdd _5_ _12_ _51_ _6_ OAI21X1
XNAND3X1_2 _51_ vdd gnd _9_ _8_ _52_ NAND3X1
XNAND2X1_7 vdd _53_ gnd D<3> _29_ NAND2X1
XOAI21X1_8 gnd vdd Q_clk<2> _7_ _54_ _6_ OAI21X1
XNAND3X1_3 Q_clk<3> vdd gnd _5_ _12_ _55_ NAND3X1
XNAND3X1_4 _55_ vdd gnd _14_ _54_ _56_ NAND3X1
XNAND3X1_5 _53_ vdd gnd _52_ _56_ _57_ NAND3X1
XNAND2X1_8 vdd _58_ gnd _3_ _4_ NAND2X1
XOAI21X1_9 gnd vdd Q_clk<2> _58_ _59_ Q_clk<3> OAI21X1
XAOI21X1_4 gnd vdd _59_ _20_ _60_ _18_ AOI21X1
XOAI21X1_10 gnd vdd _60_ _57_ _61_ _33_ OAI21X1
XNAND2X1_9 vdd _62_ gnd _26_ _63_<3> NAND2X1
XNAND2X1_10 vdd _0_<3> gnd _62_ _61_ NAND2X1
XBUFX2_1 vdd gnd _63_<0> Q<0> BUFX2
XBUFX2_2 vdd gnd _63_<1> Q<1> BUFX2
XBUFX2_3 vdd gnd _63_<2> Q<2> BUFX2
XBUFX2_4 vdd gnd _63_<3> Q<3> BUFX2
XBUFX2_5 vdd gnd _64_ rco BUFX2
XDFFPOSX1_1 vdd _0_<0> gnd Q_clk<0> clk DFFPOSX1
XDFFPOSX1_2 vdd _0_<1> gnd Q_clk<1> clk DFFPOSX1
XDFFPOSX1_3 vdd _0_<2> gnd Q_clk<2> clk DFFPOSX1
XDFFPOSX1_4 vdd _0_<3> gnd Q_clk<3> clk DFFPOSX1
XDFFPOSX1_5 vdd _1_ gnd rco_clk clk DFFPOSX1
XINVX1_2 rco_clk _2_ vdd gnd INVX1
XNOR2X1_4 vdd _2_ gnd _64_ reset NOR2X1
XINVX1_3 Q_clk<0> _3_ vdd gnd INVX1
XNOR2X1_5 vdd _3_ gnd _63_<0> reset NOR2X1
XINVX1_4 Q_clk<1> _4_ vdd gnd INVX1
XNOR2X1_6 vdd _4_ gnd _63_<1> reset NOR2X1
XINVX2_1 vdd gnd _5_ Q_clk<2> INVX2
XNOR2X1_7 vdd _5_ gnd _63_<2> reset NOR2X1
XINVX1_5 Q_clk<3> _6_ vdd gnd INVX1
XNOR2X1_8 vdd _6_ gnd _63_<3> reset NOR2X1
XAND2X2_2 vdd gnd Q_clk<0> Q_clk<1> _7_ AND2X2
XNAND3X1_6 Q_clk<3> vdd gnd Q_clk<2> _7_ _8_ NAND3X1
XNOR3X1_1 vdd gnd modo<0> modo<1> modo<2> _9_ NOR3X1
XINVX1_6 _9_ _10_ vdd gnd INVX1
XNOR2X1_9 vdd _10_ gnd _11_ _8_ NOR2X1
XNAND2X1_11 vdd _12_ gnd Q_clk<0> Q_clk<1> NAND2X1
XINVX1_7 modo<1> _13_ vdd gnd INVX1
XNOR3X1_2 vdd gnd modo<0> _13_ modo<2> _14_ NOR3X1
XNOR2X1_10 vdd Q_clk<3> gnd _15_ Q_clk<2> NOR2X1
XNAND3X1_7 _15_ vdd gnd _12_ _14_ _16_ NAND3X1
XINVX1_8 modo<2> _17_ vdd gnd INVX1
XNAND3X1_8 _17_ vdd gnd modo<0> _13_ _18_ NAND3X1
XNOR2X1_11 vdd Q_clk<1> gnd _19_ Q_clk<0> NOR2X1
XNAND2X1_12 vdd _20_ gnd _15_ _19_ NAND2X1
XOAI21X1_11 gnd vdd _18_ _20_ _21_ _16_ OAI21X1
XOAI21X1_12 gnd vdd _11_ _21_ _22_ enable OAI21X1
XNAND2X1_13 vdd _23_ gnd enable _13_ NAND2X1
XAOI21X1_5 gnd vdd _17_ enable _24_ _2_ AOI21X1
XOAI21X1_13 gnd vdd modo<0> _23_ _25_ _24_ OAI21X1
XAOI21X1_6 gnd vdd _22_ _25_ _1_ reset AOI21X1
XINVX1_9 enable _26_ vdd gnd INVX1
XNAND2X1_14 vdd _27_ gnd _26_ _63_<0> NAND2X1
XNAND2X1_15 vdd _28_ gnd modo<0> modo<1> NAND2X1
XNOR2X1_12 vdd _28_ gnd _29_ modo<2> NOR2X1
XNOR2X1_13 vdd modo<2> gnd _30_ Q_clk<0> NOR2X1
.ends contador_cond
 