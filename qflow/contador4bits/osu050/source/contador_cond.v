module contador_cond(
    input clk,
    input enable,
    input [3:0] D,
    input [2:0] modo,
    input reset,
    output reg [3:0] Q,
    output reg rco
    );

    reg [3:0] Q_clk;
    reg rco_clk;

    always @(posedge clk) begin

        if (reset) begin
            Q_clk <= 0;
            rco_clk <= 0;
        end else begin

            if (enable) begin
        
                case (modo)

                    0: begin
                        if (Q_clk == 4'b1111) rco_clk <= 1;
                        else rco_clk <= 0;
                        Q_clk <= Q_clk+1;
                    end

                    1:begin
                        if (Q_clk == 4'b0000) rco_clk <= 1;
                        else rco_clk <= 0;
                        Q_clk <= Q_clk-1;
                    end

                    2:begin
                        if (Q_clk == 0 || Q_clk == 1 || Q_clk == 2) rco_clk <= 1;
                        else rco_clk <= 0;
                        Q_clk <= Q_clk-3;
                    end

                    3:begin
                        Q_clk <= D;
                        rco_clk <= 0;
                    end

                    4:begin
                        Q_clk <= 0;
                        rco_clk <= 0;
                    end

                    default: 
                        Q_clk <= 0;

                endcase
        
            end else begin
                Q_clk<=Q_clk;
                rco_clk<=rco_clk;
            end
            
        end    

	end

    always @(*) begin
        if (reset) begin
            Q = 0;
            rco = 0;
        end else begin
            Q = Q_clk;
            rco = rco_clk;
        end
    end

endmodule // contador