#!/usr/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/fabian/Documentos/microe/proyecto/qflow/contador4bits/osu050
#-------------------------------------------

set projectpath=/home/fabian/Documentos/microe/proyecto/qflow/contador4bits/osu050
set techdir=/usr/share/qflow/tech/osu050
set sourcedir=/home/fabian/Documentos/microe/proyecto/qflow/contador4bits/osu050/source
set synthdir=/home/fabian/Documentos/microe/proyecto/qflow/contador4bits/osu050/synthesis
set layoutdir=/home/fabian/Documentos/microe/proyecto/qflow/contador4bits/osu050/layout
set techname=osu050
set scriptdir=/usr/lib/qflow/scripts
set bindir=/usr/lib/qflow/bin
set synthlog=/home/fabian/Documentos/microe/proyecto/qflow/contador4bits/osu050/synth.log
#-------------------------------------------

