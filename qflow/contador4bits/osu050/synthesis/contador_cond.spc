*SPICE netlist created from BLIF module contador_cond by blif2BSpice
.include /usr/share/qflow/tech/osu050/osu050_stdcells.sp
.subckt contador_cond vdd gnd clk enable D<0> D<1> D<2> D<3> modo<0> modo<1> modo<2> reset Q<0> Q<1> Q<2> Q<3> rco 
XNAND2X1_1 vdd _28_ gnd _27_ _66_<0> NAND2X1
XNAND2X1_2 vdd _29_ gnd modo<0> modo<1> NAND2X1
XNOR2X1_1 vdd _29_ gnd _30_ modo<2> NOR2X1
XNOR2X1_2 vdd modo<2> gnd _31_ Q_clk<0> NOR2X1
XAOI22X1_1 gnd vdd _30_ D<0> _32_ _29_ _31_ AOI22X1
XNOR2X1_3 vdd _25_ gnd _33_ reset NOR2X1
XINVX1_1 _33_ _34_ vdd gnd INVX1
XOAI21X1_1 gnd vdd _32_ _34_ _0_<0> _28_ OAI21X1
XNOR2X1_4 vdd _7_ gnd _35_ _19_ NOR2X1
XNAND2X1_3 vdd _36_ gnd D<1> _30_ NAND2X1
XOAI21X1_2 gnd vdd _18_ _35_ _37_ _36_ OAI21X1
XAOI21X1_1 gnd vdd _12_ _35_ _38_ _37_ AOI21X1
XNAND2X1_4 vdd _39_ gnd _27_ _66_<1> NAND2X1
XOAI21X1_3 gnd vdd _34_ _38_ _0_<1> _39_ OAI21X1
XNAND2X1_5 vdd _40_ gnd Q_clk<0> Q_clk<1> NAND2X1
XXNOR2X1_1 _40_ Q_clk<2> gnd vdd _41_ XNOR2X1
XNAND3X1_1 Q_clk<1> vdd gnd Q_clk<0> Q_clk<2> _42_ NAND3X1
XOAI21X1_4 gnd vdd _3_ _4_ _43_ _5_ OAI21X1
XAOI22X1_2 gnd vdd _43_ _42_ _44_ modo<1> _12_ AOI22X1
XAOI21X1_2 gnd vdd _10_ _41_ _45_ _44_ AOI21X1
XNOR3X1_1 vdd gnd modo<1> _23_ modo<2> _46_ NOR3X1
XAOI21X1_3 gnd vdd _3_ _4_ _47_ _5_ AOI21X1
XNOR3X1_2 vdd gnd Q_clk<1> Q_clk<2> Q_clk<0> _48_ NOR3X1
XOAI21X1_5 gnd vdd _47_ _48_ _49_ _46_ OAI21X1
XNAND2X1_6 vdd _50_ gnd D<2> _30_ NAND2X1
XNAND2X1_7 vdd _51_ gnd _50_ _49_ NAND2X1
XOAI21X1_6 gnd vdd _51_ _45_ _52_ _33_ OAI21X1
XNAND2X1_8 vdd _53_ gnd _27_ _66_<2> NAND2X1
XNAND2X1_9 vdd _0_<2> gnd _53_ _52_ NAND2X1
XOAI21X1_7 gnd vdd _5_ _40_ _54_ _6_ OAI21X1
XNAND3X1_2 _54_ vdd gnd _9_ _8_ _55_ NAND3X1
XNAND2X1_10 vdd _56_ gnd D<3> _30_ NAND2X1
XNOR3X1_3 vdd gnd modo<0> _17_ modo<2> _57_ NOR3X1
XOAI21X1_8 gnd vdd Q_clk<2> _7_ _58_ _6_ OAI21X1
XNAND3X1_3 _5_ vdd gnd Q_clk<3> _40_ _59_ NAND3X1
XNAND3X1_4 _57_ vdd gnd _59_ _58_ _60_ NAND3X1
XNAND3X1_5 _56_ vdd gnd _55_ _60_ _61_ NAND3X1
XOAI21X1_9 gnd vdd _6_ _48_ _62_ _20_ OAI21X1
XAND2X2_1 vdd gnd _62_ _46_ _63_ AND2X2
XOAI21X1_10 gnd vdd _63_ _61_ _64_ _33_ OAI21X1
XNAND2X1_11 vdd _65_ gnd _27_ _66_<3> NAND2X1
XNAND2X1_12 vdd _0_<3> gnd _65_ _64_ NAND2X1
XBUFX2_1 vdd gnd _66_<0> Q<0> BUFX2
XBUFX2_2 vdd gnd _66_<1> Q<1> BUFX2
XBUFX2_3 vdd gnd _66_<2> Q<2> BUFX2
XBUFX2_4 vdd gnd _66_<3> Q<3> BUFX2
XBUFX2_5 vdd gnd _67_ rco BUFX2
XDFFPOSX1_1 vdd _0_<0> gnd Q_clk<0> clk DFFPOSX1
XDFFPOSX1_2 vdd _0_<1> gnd Q_clk<1> clk DFFPOSX1
XDFFPOSX1_3 vdd _0_<2> gnd Q_clk<2> clk DFFPOSX1
XDFFPOSX1_4 vdd _0_<3> gnd Q_clk<3> clk DFFPOSX1
XDFFPOSX1_5 vdd _1_ gnd rco_clk clk DFFPOSX1
XINVX1_2 rco_clk _2_ vdd gnd INVX1
XNOR2X1_5 vdd _2_ gnd _67_ reset NOR2X1
XINVX1_3 Q_clk<0> _3_ vdd gnd INVX1
XNOR2X1_6 vdd _3_ gnd _66_<0> reset NOR2X1
XINVX1_4 Q_clk<1> _4_ vdd gnd INVX1
XNOR2X1_7 vdd _4_ gnd _66_<1> reset NOR2X1
XINVX1_5 Q_clk<2> _5_ vdd gnd INVX1
XNOR2X1_8 vdd _5_ gnd _66_<2> reset NOR2X1
XINVX1_6 Q_clk<3> _6_ vdd gnd INVX1
XNOR2X1_9 vdd _6_ gnd _66_<3> reset NOR2X1
XAND2X2_2 vdd gnd Q_clk<0> Q_clk<1> _7_ AND2X2
XNAND3X1_6 Q_clk<3> vdd gnd Q_clk<2> _7_ _8_ NAND3X1
XNOR3X1_4 vdd gnd modo<0> modo<1> modo<2> _9_ NOR3X1
XINVX1_7 _9_ _10_ vdd gnd INVX1
XNOR2X1_10 vdd _10_ gnd _11_ _8_ NOR2X1
XNOR2X1_11 vdd modo<0> gnd _12_ modo<2> NOR2X1
XNAND2X1_13 vdd _13_ gnd modo<1> _12_ NAND2X1
XNOR2X1_12 vdd Q_clk<3> gnd _14_ Q_clk<2> NOR2X1
XOAI21X1_11 gnd vdd _3_ _4_ _15_ _14_ OAI21X1
XINVX1_8 modo<2> _16_ vdd gnd INVX1
XINVX1_9 modo<1> _17_ vdd gnd INVX1
XNAND3X1_7 _16_ vdd gnd modo<0> _17_ _18_ NAND3X1
XNOR2X1_13 vdd Q_clk<1> gnd _19_ Q_clk<0> NOR2X1
XNAND2X1_14 vdd _20_ gnd _14_ _19_ NAND2X1
XOAI22X1_1 gnd vdd _15_ _13_ _18_ _20_ _21_ OAI22X1
XOAI21X1_12 gnd vdd _21_ _11_ _22_ enable OAI21X1
XINVX1_10 modo<0> _23_ vdd gnd INVX1
XNAND3X1_8 _23_ vdd gnd enable _17_ _24_ NAND3X1
XNAND2X1_15 vdd _25_ gnd enable _16_ NAND2X1
XNAND3X1_9 _25_ vdd gnd rco_clk _24_ _26_ NAND3X1
XAOI21X1_4 gnd vdd _22_ _26_ _1_ reset AOI21X1
XINVX1_11 enable _27_ vdd gnd INVX1
.ends contador_cond
 