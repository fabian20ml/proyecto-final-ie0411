task checker;

	input integer iteration;

	repeat (iteration) @ (posedge clk) begin
    	if ({sb.sb_rco,sb.sb_Q} == {rco, Q}) begin
			$fdisplay(log, "PASS");
		end else begin
			$fdisplay(log, "Time=%.0f Error dut: rco=%b, Q=%d, scoreboard: rco=%b, Q=%d", $time, rco, Q, sb.sb_rco,sb.sb_Q);
		end
	end
endtask