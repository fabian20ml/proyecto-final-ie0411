`include "clk.v"
`include "cmos_cells.v"
`include "contador_cond.v"
`include "contador_estr.v"
`include "contador_tb.v"

module tb_top;

	//wire /*AUTOWIRE*/;
	// Beginning of automatic wires (for undeclared instantiated-module outputs)
	wire [3:0]	D;			// From scoreboard of contador_tb.v
	wire [3:0]	Q;			// From contador4bits of contador_cond.v
	wire		clk;			// From clocks of clk.v
	wire		enable;			// From scoreboard of contador_tb.v
	wire [2:0]	modo;			// From scoreboard of contador_tb.v
	wire		rco;			// From contador4bits of contador_cond.v
	wire		reset;			// From scoreboard of contador_tb.v
	// End of automatics

	clk clocks (
		    // Outputs
		    .clk		(clk));

	contador_tb scoreboard(
			       // Outputs
			       .enable		(enable),
			       .reset		(reset),
			       .modo		(modo[2:0]),
			       .D		(D[3:0]),
			       // Inputs
			       .rco		(rco),
			       .clk		(clk),
			       .Q		(Q[3:0]));

	/*contador_cond contador4bits_cond(
				    // Outputs
				    .Q			(Q[3:0]),
				    .rco		(rco),
				    // Inputs
				    .clk		(clk),
				    .enable		(enable),
				    .D			(D[3:0]),
				    .modo		(modo[2:0]),
				    .reset		(reset));*/

	contador_estr contador4bits_estr(/*AUTOINST*/
					 // Outputs
					 .Q			(Q[3:0]),
					 .rco			(rco),
					 // Inputs
					 .D			(D[3:0]),
					 .clk			(clk),
					 .enable		(enable),
					 .modo			(modo[2:0]),
					 .reset			(reset));


endmodule





