`include "../contador4bits/contador_cond.v"

module contador32_cond(
    input clk,
    input enable,
    input [31:0] D,
    input [2:0] modo,
    input reset,
    output [31:0] Q,
    output rco);

    wire clk2, clk3, clk4, clk5, clk6, clk7, clk8;
    wire rco1, rco2, rco3, rco4, rco5, rco6, rco7, rco8;
    wire checkmodo_10, reset_modo3_modo4;
    wire [2:0] modo_10;

    assign checkmodo_10 = (!modo[0])&(modo[1]);
    assign reset_modo3_modo4 = reset|(modo[1]&modo[0])|(modo[2]);
    //assign reset_modo3_modo4 = reset|((!reset)&((modo[1]&modo[0])|(modo[2])));
    assign rco = rco1 & rco2 & rco3 & rco4 & rco5 & rco6 & rco7 & rco8;

    assign modo_10 = (checkmodo_10) ? 3'b001 : modo;
    assign clk2 = (reset_modo3_modo4) ? clk : rco1;
    assign clk3 = (reset_modo3_modo4) ? clk : rco2;
    assign clk4 = (reset_modo3_modo4) ? clk : rco3;
    assign clk5 = (reset_modo3_modo4) ? clk : rco4;
    assign clk6 = (reset_modo3_modo4) ? clk : rco5;
    assign clk7 = (reset_modo3_modo4) ? clk : rco6;
    assign clk8 = (reset_modo3_modo4) ? clk : rco7;
    

    contador_cond counter1(clk, enable, D[3:0], modo, reset, Q[3:0], rco1);

    contador_cond counter2(clk2, enable, D[7:4], modo_10, reset, Q[7:4], rco2);

    contador_cond counter3(clk3, enable, D[11:8], modo_10, reset, Q[11:8], rco3);

    contador_cond counter4(clk4, enable, D[15:12], modo_10, reset, Q[15:12], rco4);

    contador_cond counter5(clk5, enable, D[19:16], modo_10, reset, Q[19:16], rco5);

    contador_cond counter6(clk6, enable, D[23:20], modo_10, reset, Q[23:20], rco6);

    contador_cond counter7(clk7, enable, D[27:24], modo_10, reset, Q[27:24], rco7);

    contador_cond counter8(clk8, enable, D[31:28], modo_10, reset, Q[31:28], rco8);
    
endmodule