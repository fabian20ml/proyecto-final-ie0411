//Init task for clean output signals
task drv_init;
	begin
	@(negedge clk)
		{reset,modo,enable,D} <= 37'h1800000000;
	@(negedge clk)
		{reset,modo,enable,D} <= 37'h0800000000;
	@(negedge clk)
		{reset,modo,enable,D} <= 37'h0800000000;
	end
endtask

//Prueba0: cuenta hacia arriba (MODO 000)
task Prueba0;

  input integer iteration;

    repeat (iteration) begin  
		@(negedge clk) begin
			enable <= 1;
			modo <= 0;
		end
    end

endtask

//Prueba1: cuenta hacia abajo (MODO 001)
task Prueba1;

  input integer iteration;

    repeat (iteration) begin  
		@(negedge clk) begin
			enable <= 1;
			modo <= 1;
		end
    end

endtask

//Prueba2: cuenta de tres en tres hacia abajo (MODO 010)
task Prueba2;

  input integer iteration;

    repeat (iteration) begin  
		@(negedge clk) begin
			enable <= 1;
			modo <= 2;
		end
    end

endtask

//Prueba3: carga en paralelo (MODO 011)
task Prueba3;

  input integer iteration;

    repeat (iteration) begin  
		@(negedge clk) begin
			enable <= 1;
			modo <= 3;
			D <= $urandom;
		end
    end

endtask

//Prueba4: la entrada "modo" cambia aleatoriamente
task Prueba4;

  input integer iteration;

    repeat (iteration) begin  
		@(negedge clk) begin
			enable <= 1;
			modo <= $urandom%5;
			D <= $urandom;
		end
    end

endtask

//Prueba5: todas las entradas cambian aleatoriamente
task Prueba5;

  input integer iteration;

    repeat (iteration) begin  
		@(negedge clk) begin
			enable <= $random;
			modo <= $urandom%5;
			D <= $urandom;
		end
    end

endtask

task turnOff;
	begin
		@(negedge clk)
		{enable,reset} <= 2'b00;
		@(negedge clk)
		{enable,reset} <= 2'b00;
		@(negedge clk)
		{enable,reset} <= 2'b01;
		@(negedge clk)
		{enable,reset} <= 2'b01;
	end
endtask