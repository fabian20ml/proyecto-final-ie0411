`include "clk.v"
`include "cmos_cells.v"
`include "contador32_cond.v"
`include "contador32_estr.v"
`include "contador_tb.v"

module tb_top;

	//wire /*AUTOWIRE*/;
	// Beginning of automatic wires (for undeclared instantiated-module outputs)
	wire [31:0]	D;			// From scoreboard of contador_tb.v
	wire [31:0]	Q;			// From counter32 of contador32_cond.v, ...
	wire		clk;			// From clocks of clk.v
	wire		enable;			// From scoreboard of contador_tb.v
	wire [2:0]	modo;			// From scoreboard of contador_tb.v
	wire		rco;			// From counter32 of contador32_cond.v, ...
	wire		reset;			// From scoreboard of contador_tb.v
	// End of automatics

	clk clocks (
		    // Outputs
		    .clk		(clk));

	contador_tb scoreboard(
			       // Outputs
			       .enable		(enable),
			       .reset		(reset),
			       .modo		(modo[2:0]),
			       .D		(D[31:0]),
			       // Inputs
			       .rco		(rco),
			       .clk		(clk),
			       .Q		(Q[31:0]));

	/*contador32_cond counter32(
				  // Outputs
				  .Q			(Q[31:0]),
				  .rco			(rco),
				  // Inputs
				  .clk			(clk),
				  .enable		(enable),
				  .D			(D[31:0]),
				  .modo			(modo[2:0]),
				  .reset		(reset));*/

	contador32_estr counter32_synth(
					// Outputs
					.Q		(Q[31:0]),
					.rco		(rco),
					// Inputs
					.D		(D[31:0]),
					.clk		(clk),
					.enable		(enable),
					.modo		(modo[2:0]),
					.reset		(reset));


endmodule





