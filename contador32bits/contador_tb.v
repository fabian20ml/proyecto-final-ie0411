// Testbench Code Goes here
`include "scoreboard.v"

module contador_tb(clk, enable, D, modo, reset, Q, rco);

output reg  enable, reset; 
output reg [2:0] modo;
output reg [31:0] D;
input rco, clk;
input [31:0] Q;

`include "driver.v"
`include "checker.v"

parameter ITERATIONS = 100;
integer log;

initial begin

  $dumpfile("contador_wf.vcd");
  $dumpvars(0);

  log = $fopen("tb.log");
  $fdisplay(log, "time=%5d, Simulation Start", $time);
  $fdisplay(log, "time=%5d, Starting Reset", $time);

  drv_init();

  $fdisplay(log, "time=%5d, Reset Completed", $time);

  $fdisplay(log, "time=%5d, Starting Test", $time);
  fork
    Prueba0(ITERATIONS); // Pruebas 0 - 5
    checker(ITERATIONS);
    
  join
  turnOff();
  $fdisplay(log, "time=%5d, Test Completed", $time);
  $fdisplay(log, "time=%5d, Simulation Completed", $time);
  $fclose(log);
  #200 $finish;
end

scoreboard sb(
	      // Inputs
	      .clk			(clk),
	      .enable			(enable),
	      .D			(D),
	      .modo			(modo),
        .reset        (reset));

endmodule
