//Scoreboard
module scoreboard (clk, enable, D, modo, reset);

input  clk, enable, reset;
input [2:0] modo;
input [31:0] D;

wire  sb_rco;
wire [31:0] sb_Q;
reg    [31:0] sb_counter;
reg    sb_carry;

assign  sb_rco = sb_carry;
assign  sb_Q = sb_counter;

//This code do not need to be RTL. No synthesis rules.
	always @(posedge clk) begin

		if (enable && !reset) begin

			sb_carry <= 0;
			
			case (modo)

				0: begin
					if (sb_counter == 32'hFFFFFFFF) begin
						sb_carry <= 1;
					end
					sb_counter <= sb_counter+1;
				end

				1:begin
					if (sb_counter == 0) begin
						sb_carry <= 1;
					end
					sb_counter <= sb_counter-1;
				end

				2:begin
					if (sb_counter == 0 || sb_counter == 1 || sb_counter == 2) begin
						sb_carry <= 1;
					end
					sb_counter <= sb_counter-3;
				end

				3:begin
					sb_counter <= D;
					sb_carry<=0;
				end

				4:begin
					sb_counter <= 0;
					sb_carry<= 0;
				end

				default: 
					sb_counter <= 0;

			endcase
			

		end else if (!enable & reset) begin
			sb_counter <=  0;
			sb_carry <= 0;
		end else begin
			sb_counter <=  sb_counter;
			sb_carry <= sb_carry;
		end
			
		
	end

	always @(reset) begin
		if (reset) begin
			sb_counter = 0;
			sb_carry = 0;
		end
	end

endmodule

